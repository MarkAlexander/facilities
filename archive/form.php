<p>The Sanctions Risk Matrix will calculate whether your risk requires secondary or underlying beneficiaries of the policy to be sanctioned checked.
   A score of 1 or more requires a secondary and underlying beneficial owners to be sanctioned checked.
   <a href="files/Beneficial Owners for Deep DIve Sanctions25032015.xlsx">Please see attached list of secondary and beneficial owners</a>.</p>

<div class="panel panel-default panel-score" >
  <div class="panel-heading">
    <h3 class="panel-title">Current Risk</h3>
  </div>
  <div class="panel-body">
    <p class="score">-</p>
    <p><strong class="status"></strong></p>
  </div>
</div>

<div class="panel panel-default panel-risk-matrix">
  <div class="panel-heading">
    <h3 class="panel-title">Risk Matrix</h3>
  </div>
  <div class="panel-body">
    <form id="risk-matrix-form" role="form" action="index.php" method="post">
      <div class="form-group">
        <label for="insured" class="control-label">Name of the insured:</label>
        <label for="insured" class="error"></label>
        <input name="insured" class="form-control input-lg" type="text" required />
      </div>
      
      <div class="territories form-group form-horizontal">
        <label>Territories</label>
        <p class="text-info">This allocation should be done using an exposure measure that is relevant to the risk (e.g. values for property, revenues for liability...)  For marine risks, political risks or trade credit you can use the domicile of the insured. However, if you are aware that voyages or underlying assets are associated with territories with a high sanctions risk you should contact Business Risk for guidance.</p>
        <p class="text-info">For risks that are exposed to more than one territory, you can allocate a percentage of the risk to each territory. In this case, the percentages shown should add up to 100.</p>
        <div class="row">
          <div class="col-xs-1">
            <div class="btn-group btn-group-xs">
              <button type="button" class="del-territory btn btn-default" title="Remove last territory" disabled>
                <span class="glyphicon glyphicon-minus"></span>
              </button>
              <button type="button" class="add-territory btn btn-default" title="Add new territory">
                <span class="glyphicon glyphicon-plus"></span>
              </button>
            </div>
          </div>
          <div class="col-xs-6">
            <label for="territories[]">Territory</label>
            <label for="territories[]" class="error"></label>
            <input type="hidden" name="emptyTerritories" id="emptyTerritories" value="0" />
            <label for="emptyTerritories" class="error"></label>
          </div>
          <div class="col-xs-5 weight">
            <label for="territoryWeights[]">Weight</label>
            <label for="territoryWeights[]" class="error"></label>
            <input type="hidden" name="totalWeight" id="totalWeight" value="100" />
            <label for="totalWeight" class="error"></label>
          </div>
        </div>
      </div>
      
      <div class="cob form-group">
        <label for="cob" class="control-label">COB</label>
        <label for="cob" class="error"></label>
        <p class="text-info">If more than one item applies, select the highest scoring item only.</p>
        <select name="cob" class="form-control" required>
          <option></option>
          <option value="0">Professional Indemnity (0)</option>
          <option value="0">Directors &amp; Officers (0)</option>
          <option value="0.1">Trade Credit (0.1)</option>
          <option value="0.3">Property / Liability (Non Marine) (0.3)</option>
          <option value="0.3">Motor (0.3)</option>
          <option value="0.3">Personal Accident (0.3)</option>
          <option value="0.3">Contingency (0.3)</option>
          <option value="0.3">Political Risk / Structured Credit (0.3)</option>
          <option value="0.4">Energy (0.4)</option>
          <option value="0.2">Marine (including cargo) - EEA, North America or Australasia or other territories with a CPI of 6 or more (0.2)</option>
          <option value="0.4">Marine (including cargo) - outside EEA, North America or Australasia or other territories with a CPI of 6 or less (0.4)</option>
          <option value="0.5">Specie (0.5)</option>
          <option value="0.5">Fine Art (0.5)</option>
        </select>
      </div>
      
      <div class="hrlob form-group">
        <label for="hrlob" class="control-label">High Risk LOB</label>
        <label for="hrlob" class="error"></label>
        <p class="text-info">Does your risk include any of the below high risk lines of business?</p>
        <select id="hrlob" name="hrlob" class="form-control" required>
          <option></option>
          <option value="0">No</option>
          <option value="0.1">Oil or Gas Equipment, Services or Distribution</option>
          <option value="0.2">Gas Producers</option>
          <option value="0.2">Oil Producers</option>
          <option value="0.3">Oil &amp; Gas Exploration</option>
          <option value="0.3">Construction</option>
          <option value="0.4">Mining</option>
          <option value="0.8">Military Hardware</option>
          <option value="0.5">Potential Dual Use materials to embargoed territory</option>
        </select>
      </div>
      
      <div class="ownership form-group">
        <label for="ownership" class="control-label">Ownership</label>
        <label for="ownership" class="error"></label>
        <select id="ownership" name="ownership" class="form-control" required>
          <option></option>
          <option value="0.3">Individual / Sole Trader</option>
          <option value="0.2">Privately Owned</option>
		  <option value="0.2">Not Known</option>
          <option value="0.1">Publicly Listed</option>
          <option value="0">State Owned or Controlled</option>
        </select>
      </div>
      
      <div class="form-group">
        <input name="submit" class="btn btn-primary" type="submit" value="Preview" />
      </div>
    </form>
  </div>
</div>

<div class="alert alert-warning" role="alert">
  <p class="hidden-print">Any questions should be directed to Business Risk.</p>
</div>
