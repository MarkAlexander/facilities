<?php

session_start();

if ( isset( $_SESSION[ 'RiskMatrix' ] ) ) {
  
  // Generate PDF
  require_once( 'tcpdf/tcpdf.php' );
  $pdf = new TCPDF( PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false );
  // set document information
  $pdf->SetCreator( PDF_CREATOR );
  $pdf->SetAuthor( 'RKH/Sanctions Checker' );
  $insured = $_SESSION[ 'RiskMatrix' ][ 'input' ][ 'insured' ];
  $pdf->SetTitle( $insured . ' Sanctions Check' );
  $pdf->SetSubject( 'Sanctions Check' );
  $pdf->SetKeywords( 'Sanctions, Check, Risk' );
  // add content
  $pdf->AddPage();
  $pdf->writeHTMLCell( 0, 0, '', '', $_SESSION[ 'RiskMatrix' ][ 'summary' ], 0, 1, 0, true, '', true );
  require_once 'inc/sanitize.php';
  $filename = sanitize_filename( $insured ) . '_Sanctions_Check' . '.pdf';
  
  // Send it by email or download it
  if ( isset($_GET[ 'email' ] ) ) {
    
    // get user email
    require_once 'inc/useremail.php';
    $useremail = getUserMail();
    
    $from = $useremail;
    $mailto = $useremail;
    $subject = "Risk Matrix - " . $insured;
    
    // get the pdf content as an attachment
    $pdfcontent = $pdf->Output( $filename, 'E' );
    // a random hash will be necessary to send mixed content
    $separator = md5( date( 'r', time() ) );
    
    // main header (multipart mandatory)
    $headers  = "From: " . $from . PHP_EOL;
    $headers .= "MIME-Version: 1.0" . PHP_EOL;
    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . PHP_EOL . PHP_EOL;
    $headers .= "Content-Transfer-Encoding: 7bit" . PHP_EOL;
    $headers .= "This is a MIME encoded message." . PHP_EOL . PHP_EOL;

    // message
    $message .= "--" . $separator . PHP_EOL;
    $message .= "Content-Type: text/plain; charset=\"iso-8859-1\"" . PHP_EOL;
    $message .= "Content-Transfer-Encoding: 8bit" . PHP_EOL . PHP_EOL;
    $message .= "Find attached the PDF for the following Risk Matrix validation:" . PHP_EOL . PHP_EOL;
    $message .= "  Insured:       " . $insured . PHP_EOL;
    $message .= "  Report dated:  " . $_SESSION[ 'RiskMatrix' ][ 'date' ] . PHP_EOL;
    $message .= "  Score:         " . $_SESSION[ 'RiskMatrix' ][ 'score' ] . PHP_EOL . PHP_EOL;

    // attachment
    $message .= "--" . $separator . PHP_EOL;
    $message .= $pdfcontent . PHP_EOL . PHP_EOL;
    $message .= "--" . $separator . "--" . PHP_EOL;

    // Send Mail
    $mailsent = mail( $mailto, $subject, $message, $headers );
    
    if ($mailsent) {
      $_SESSION[ 'RiskMatrix' ][ 'sentMail' ] = true;
    } else {
      $_SESSION[ 'RiskMatrix' ][ 'sentMail' ] = false;
    }
    $_SESSION[ 'RiskMatrix' ][ 'completed' ] = true;
    header( 'Location: index.php' );
    
  } else {
    
    $_SESSION[ 'RiskMatrix' ][ 'completed' ] = true;
    $pdf->Output( $filename, 'I' );
    die();
    
  }

} else {
  
  header( 'Location: index.php' );
?>
  <p class="error">No information provided.</p>
  <p>Go <a href="index.php">back</a> and fill in the form, please.</p>
<?php
}
