  <p>There were some problems with the information provided:</p>
    <div class="error">
      <ul>
        <?php foreach ( $inputErr as $error ) : ?>
        <?php if ( !empty( $error ) ) : ?>
        <li><?php echo $error; ?></li>
        <?php endif; ?>
        <?php endforeach; ?>
      </ul>
    </div>