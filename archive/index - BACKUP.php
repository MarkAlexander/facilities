<?php
$userLogged=$_SERVER ['REMOTE_USER'];
	$dbfilename = $_SERVER["DOCUMENT_ROOT"] . '\BizCard\bizcard.mdb';
	$dbuser = "";
	$dbpassword = "";
	if (!file_exists($dbfilename)) {
		die("Could not find database file.");
	}
	$conn = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)};Dbq=$dbfilename", $dbuser, $dbpassword);
	$select = $conn->prepare("SELECT * from cardstable WHERE cardrequester = '$userLogged' ORDER BY ID DESC");
	$select->execute();
	?>
	
<?php

include 'datalogin.php'; 
$whoisit =  $_SERVER['REMOTE_USER'];
IF (preg_match('/^r/i', $whoisit))
{$whoisit2 = substr($whoisit, 4); 
$whoisit3 = str_replace("'","''",$whoisit2);}

IF (preg_match('/^h/i', $whoisit))
{$whoisit2 = substr($whoisit, 9); 
$whoisit3 = str_replace("'","''",$whoisit2);}
$result = mysqli_query($con,"SELECT * FROM intranetusers WHERE loginname = '$whoisit3'");
while($row = mysqli_fetch_array($result))
{
IF ($_POST['ddi'] == "") { $userddi = $row['ddi'];} ELSE {$userddi = $_POST['ddi'];}
IF ($_POST['email'] == "") { $useremail = $row['useremail'];} ELSE {$useremail = $_POST['email'];}
IF ($_POST['mobile'] == "") { $usermobile = $row['mobile'];} ELSE {$usermobile = $_POST['mobile'];}
IF ($_POST['Name'] == "") { $username = $row['firstname'] . " " . $row['lastname'];} ELSE {$username = $_POST['Name'];}
IF ($_POST['jobtitle'] == "") { $userjobtitle = $row['jobtitle'];} ELSE {$userjobtitle = $_POST['jobtitle'];}
IF ($_POST['location'] == "") { $userlocation = $row['locationid'];} ELSE {$userlocation = $_POST['location'];}
IF ($_POST['ddi'] == "") { $usercompany = $row['usercomp'];} ELSE {$usercompany = $_POST['ddi'];}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Business Card Ordering System</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Risk Matrix</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- <link href="css/bootstrap-theme.min.css" rel="stylesheet"> -->
  <link href="css/styles.css" rel="stylesheet">
<script type="text/javascript" src="//use.typekit.net/vjr8xic.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/bootstrap.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
</head>

<body>
<div class="container" style="width: 100%;">


<?php

$ref = $_POST['cardack'];
if ($ref == "Thanks") {
   echo '<script language="javascript">';
echo 'alert("Thank you for your order, you will shortly receive an email confirming details of the card requested.")';
echo '</script>';
}
?>
<h1><img border="0" src="img/modulalogo.png" width="150">&nbsp; Business Card Ordering System</h1>

<div class="panel panel-default panel-preview" style="width:35%; float: left; min-width: 300px; max-width: 500px; margin-right: 20px;"">
      <div class="panel-heading">
        <h3 class="panel-title">Entry Form</h3>
      </div>
	  
	  
      <div class="panel-body">
      <B>Business card orders are collated by our printers and printed on the 1st of every month. Delivery is normally 5 to 7 working days after this date.</B> If there is an urgent need for business cards, please add this to the additional notes section or contact <a href="mailto:michael.b@modula.co">michael.b@modula.co</a> directly.
	  <BR><BR> 
		
		<form method="POST" action="index.php">
							<p>
							<label class="sr-only" for="company">Brand</label>
							<select name="company" id="company" class="form-control" style="width: 100% !important; color: #808080; padding: 0 6px;" required ">
							<option value="">Brand *</option>
							<option <?PHP IF ($_POST['company'] == "Dual Private Client") { echo "selected"; } ?> value="Dual Private Client">Dual Private Client</option>
							<option <?PHP IF ($_POST['company'] == "Dual Private Client Aurum") { echo "selected"; } ?> value="Dual Private Client Aurum">Dual Private Client Aurum</option>
							<option <?PHP IF ($_POST['company'] == "Bowood Insurance Brokers") { echo "selected"; } ?> value="Bowood Insurance Brokers">Bowood Insurance Brokers</option>
							<option <?PHP IF ($_POST['company'] == "BPIF") { echo "selected"; } ?> value="BPIF">BPIF</option>
							<option <?PHP IF ($_POST['company'] == "CLAIS") { echo "selected"; } ?> value="CLAIS">CLAIS</option>
							<!-- <option <?PHP IF ($_POST['company'] == "Dual") { echo "selected"; } ?> value="Dual">Dual</option> -->
							<option <?PHP IF ($_POST['company'] == "GCS" OR $usercompany == "8") { echo "selected"; } ?> value="GCS">GCS</option>
							<option <?PHP IF ($_POST['company'] == "Howden") { echo "selected"; } ?> value="Howden">Howden</option>
							<!-- <option <?PHP IF ($_POST['company'] == "Howden CHIS Care") { echo "selected"; } ?> value="Howden CHIS Care">Howden CHIS Care</option> -->
							<option <?PHP IF ($_POST['company'] == "Howden Care") { echo "selected"; } ?> value="Howden Care">Howden Care</option>
							<!-- <option <?PHP IF ($_POST['company'] == "Howden Prime Care") { echo "selected"; } ?> value="Howden Prime Care">Howden Prime Care</option> -->
							<!-- <option <?PHP IF ($_POST['company'] == "Hyperion") { echo "selected"; } ?> value="Hyperion" >Hyperion</option> -->
							<option <?PHP IF ($_POST['company'] == "NHF") { echo "selected"; } ?> value="NHF">NHF Insurance</option>
							<option <?PHP IF ($_POST['company'] == "RKH Reinsurance") { echo "selected"; } ?> value="RKH Reinsurance">RKH Reinsurance Brokers</option>
							<option <?PHP IF ($_POST['company'] == "RKH Specialty (Generic)") { echo "selected"; } ?> value="RKH Specialty (Generic)">RKH Specialty (Generic)</option>
							<option <?PHP IF ($_POST['company'] == "RKH Specialty Financial Risks") { echo "selected"; } ?> value="RKH Specialty Financial Risks">RKH Specialty Financial Risks</option>
							<option <?PHP IF ($_POST['company'] == "RKH Specialty Marine Energy Construction") { echo "selected"; } ?> value="RKH Specialty Marine Energy Construction">RKH Specialty Marine, Energy & Construction</option>
							<option <?PHP IF ($_POST['company'] == "RKH Specialty Property Casualty") { echo "selected"; } ?> value="RKH Specialty Property Casualty">RKH Specialty Property & Casualty</option>
							<option <?PHP IF ($_POST['company'] == "RKH Specialty Risk Solutions") { echo "selected"; } ?> value="RKH Specialty Risk Solutions">RKH Specialty Risk Solutions</option>
							<option <?PHP IF ($_POST['company'] == "R K Harrison") { echo "selected"; } ?> value="R K Harrison">R K Harrison Private Wealth</option>
							</select>
							</p><p>
							<label class="sr-only" for="location">Location</label>
							<select name="location" id="location" class="form-control" style="width: 100% !important; color: #808080; padding: 0 6px;" required value="<?PHP echo $_POST['location']; ?>">
							<option value="">Location *</option>
							<option <?PHP IF ($_POST['location'] == "Bedford" OR $userlocation == "1") { echo "selected"; } ?> value="Bedford">Bedford</option>
							<option <?PHP IF ($_POST['location'] == "Bermuda" OR $userlocation == "8") { echo "selected"; } ?> value="Bermuda">Bermuda</option>
							<option <?PHP IF ($_POST['location'] == "Birmingham" OR $userlocation == "4") { echo "selected"; } ?> value="Birmingham">Birmingham</option>
							<option <?PHP IF ($_POST['location'] == "Durrington" OR $userlocation == "9") { echo "selected"; } ?> value="Durrington">Durrington</option>
							<option <?PHP IF ($_POST['location'] == "Glasgow" OR $userlocation == "6") { echo "selected"; } ?> value="Glasgow">Glasgow</option>
							<option <?PHP IF ($_POST['location'] == "Leeds" OR $userlocation == "3") { echo "selected"; } ?> value="Leeds">Leeds</option>
							<option <?PHP IF ($_POST['location'] == "London Whittington" OR $userlocation == "2") { echo "selected"; } ?> value="London Whittington">London Whittington</option>
							<option <?PHP IF ($_POST['location'] == "London Eastcheap" OR $userlocation == "14") { echo "selected"; } ?> value="London Cullum">London Cullum</option>
							<option <?PHP IF ($_POST['location'] == "London Fenchurch" OR $userlocation == "16") { echo "selected"; } ?> value="London Fenchurch">London Fenchurch</option>
							<option <?PHP IF ($_POST['location'] == "Poole" OR $userlocation == "5") { echo "selected"; } ?> value="Poole">Poole</option>
							<option <?PHP IF ($_POST['location'] == "Richmond" OR $userlocation == "11") { echo "selected"; } ?> value="Richmond">Richmond</option>
							
							
							</select>
							</p><p>
							<label class="sr-only" for="Name">Name</label>
							<input AUTOCOMPLETE=OFF type="text" name="Name" id="Name" class="form-control" placeholder="Name *" required data-error="Mandatory Field, please complete" value="<?PHP echo $username; ?>">
							</p><p>
							<label class="sr-only" for="Qualifications">Qualifications</label>
							<input AUTOCOMPLETE=OFF type="text" name="Qualifications" id="Qualifications" class="form-control"  placeholder="Qualifications" value="<?PHP echo $_POST['Qualifications']; ?>">
							</p><p>
							<label class="sr-only" for="jobtitle">Job Title</label>
							<input AUTOCOMPLETE=OFF type="text" name="jobtitle" id="jobtitle"class="form-control"  placeholder="Job Title *"  required data-error="Mandatory Field, please complete" value="<?PHP echo $userjobtitle; ?>">
							</p><p>
							<label class="sr-only" for="division">Division</label>
							<input AUTOCOMPLETE=OFF type="text" class="form-control" for="division" name="division" placeholder="Division" value="<?PHP echo $_POST['division']; ?>" >
							</p><p>
							<label class="sr-only" for="DDI">DDI</label>
							<input type="text" name="ddi" id="ddi" class="form-control" placeholder="DDI *" required data-error="Mandatory Field, please complete" value="<?PHP echo $userddi; ?>">
							</p><p>
							<label class="sr-only" for="Mobile">Mobile</label>
							<input class="form-control" type="text" id="mobile" name="mobile"  placeholder="Mobile" value="<?PHP echo $usermobile; ?>">
							</p><p>
							<label class="sr-only" for="exampleInputEmail3">Email address</label>
							<input AUTOCOMPLETE=OFF type="text" name="email" class="form-control" id="exampleInputEmail3" placeholder="Email *" required data-error="Mandatory Field, please complete" value="<?PHP echo $useremail; ?>">
							</p>
							<p>
							<label class="sr-only" for="addnotes">Any additional notes</label>
							<input AUTOCOMPLETE=OFF type="text" name="addnotes" class="form-control" id="addnotes" placeholder="Additional notes" value="<?PHP echo $_POST['addnotes']; ?>">
							</p>
							<p style="padding: 15px 0 0 0;">
							<button type="submit" class="btn btn-primary btn-lg btn-block">Preview Card</button>
							<div>
							<input type="hidden" NAME="AddedBy" SIZE="1" VALUE=""><input type="hidden" NAME="strUSER" SIZE="1" >
							<div style="width:60%; float: left;">
								<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-prime btn" style="float: left;" data-toggle="modal" data-target="#myModal">View Orders</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Order Status and History</h4>
      </div>
      <div class="modal-body">
	  <table style='border:1pt solid black; font-size: 9pt;'><tr><td style='border:1pt solid black; background: #f2f2f2;'>Name</td><td style='border:1pt solid black; background: #f2f2f2;'>Brand</td><td style='border:1pt solid black; background: #f2f2f2;'>Date Req</td><td style='border:1pt solid black; background: #f2f2f2;'>Location</td><td style='border:1pt solid black; background: #f2f2f2;'>Status</td><td style='border:1pt solid black; background: #f2f2f2;'>Date Updated</td></tr>
	  <?php
	  	while ($row = $select->fetch(PDO::FETCH_ASSOC)) {
		$phpdate = strtotime( $row['cardrequested'] );

		
		echo "<tr><td style='border:1pt solid black;'>";
        print $row['cardname'] . "\t";
		echo "</td><td style='border:1pt solid black;'>";
        print $row['cardcompany'] . "\n";
		echo "</td><td style='border:1pt solid black;' nowrap>";
		print date("d-m-Y",$phpdate) . "\n";
		echo "</td><td style='border:1pt solid black;'>";
        print $row['cardlocation'] . "\n";
		echo "</td><td style='border:1pt solid black;'>";
        print $row['cardstatus'] . "\n";
		echo "</td><td style='border:1pt solid black;'>";
        print $row['cardstatusdate'] . "\n";
		echo "</td></tr>";
    }
	?>
	  </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
							
							</div>
							<div style="width:40%; float: left; text-align: right;"><p style="color: #808080; font-size: 8pt;">* Mandatory Fields<BR>
							<?php
							IF ($userLogged == "RKH\Mark.Bichener" OR $userLogged == "HYPERION\Mark.Bichener" OR $userLogged == "RKH\michael.banbury" OR $userLogged == "HYPERION\michael.banbury" OR $userLogged == "HYPERION\paul.hillier" OR $userLogged == "HYPERION\daniel.witcherley" OR $userLogged == "HYPERION\atif.mahmood" ) {
							echo "<a href='status.php'>Admin Area</a>";
							}?></p>
							</div>
							</div>
			</form>
			
			

	
	
	
	
	
	
	
	
	
		</div>
		
		
	</div>
	  
<!---
<div id="spacer" style="width:25px; float: left; margin: 0 0 0 0; min-width: 20px;">&nbsp;</div>
--->
<div class="panel panel-default panel-preview" style="width:65%; float: left; margin: 0 0 0 0; min-width: 400px;">
      <div class="panel-heading">
        <h3 class="panel-title">Business Card Preview</h3>
      </div>
      <div class="panel-body" style="background: #f2f2f2;" >
		<div align=center>
		
		<?php
		if ($ref == "Thanks") {
		echo '<h1><B>Thank you</B><BR>Do you wish to create another card request?</h1>';
		} 
		if ($ref !== "Thanks" AND $_POST['company'] == "") { 
		echo '<h1>Enter your details on the left to preview a card</h1>';
	    } ?>
	  
	  <?php IF ($_POST['company'] == "CLAIS") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #FFFFFF; padding: 25px; font-size: 8.5pt; color: #333333; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/clanewlogo.png" align="left" hspace="0" width="210" height="48">
					</div>
				
				<div style="Clear:both; padding: 35px 0 0 0;">
					<b><?PHP echo $_POST['Name']; ?></b>&nbsp;<?PHP echo $_POST['Qualifications']; ?>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						<?php IF ($_POST['division'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['division']; 
							}
						?>

					<br><br>
							
						<?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR>";
							echo $_POST['mobile'];
							}
						?>							
					<br>
						<?PHP echo $_POST['email']; ?>
				</div>
		</div>
		<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; background: #5B9A98; padding: 45px 0 0 0; font-size: 10pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000;" id="table51" >
							<p align="center">
							<img border="0" src="img/claback.jpg" hspace="0" width="320" height="49"></p>
							<br>
							<p align="center">
							Chosen Office Address Goes Here<br>
							t office tel no | f office fax no<br>
							<br>
							www.clainsurance.co.uk
							<br><br><br>
		</div>
	<?php
	}
	?>
	
		<?php IF ($_POST['company'] == "Howden") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #FFFFFF; padding: 25px; font-size: 8.5pt; color: #333333; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/howdenlogo.jpg" align="left" hspace="0" width="170">
					</div>
				
				<div style="Clear:both; padding: 40px 0 0 0; color: #002857; font-size: 8pt;">
					<font color="#38b5e6" style="font-size: 9pt;"><b><?PHP echo $_POST['Name']; ?></b>&nbsp;<?PHP echo $_POST['Qualifications']; ?></font>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						<?php IF ($_POST['division'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['division']; 
							}
						?>
						<br><br>
						<B>D</B>	
						<?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><B>M</B> ";
							echo $_POST['mobile'];
							}
						?>							
					<br><B>E</B>
						<?PHP echo $_POST['email']; ?>
				</div>
		</div>
				<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; height: 240px; background: #38b5e6; padding: 40px 25px 25px 25px; font-size: 7pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000;" id="table51" >
							
							<img border="0" src="img/howdenback.png" align="left" hspace="0" width="300">
						
							<br><br>
		</div>
	<?php
	}
	?>
	
	
	
	<?php IF ($_POST['company'] == "Howden Care") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #FFFFFF; padding: 25px; font-size: 8.5pt; color: #333333; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/howdenlogo.jpg" align="left" hspace="0" width="170">
					</div>
				
				<div style="Clear:both; padding: 40px 0 0 0; color: #002857; font-size: 8pt;">
					<font color="#38b5e6" style="font-size: 9pt;"><b><?PHP echo $_POST['Name']; ?></b>&nbsp;<?PHP echo $_POST['Qualifications']; ?></font>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						
						<BR>Howden Care

					<br><br>
						<B>D</B>	
						<?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><B>M</B> ";
							echo $_POST['mobile'];
							}
						?>							
					<br><B>E</B>
						<?PHP echo $_POST['email']; ?>
				</div>
		</div>
				<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; height: 240px; background: #38b5e6; padding: 25px; font-size: 7pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000;" id="table51" >
							
							<img border="0" src="img/howdenbackcare.png" align="left" hspace="0" width="300">
						
							<br><br>
		</div>
	<?php
	}
	?>
	
			<?php IF ($_POST['company'] == "R K Harrison") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #FFFFFF; padding: 25px; font-size: 8.5pt; color: #333333; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/rkhpwlogo.jpg" align="left" hspace="0" width="220">
					</div>
				
				<div style="Clear:both; padding: 30px 0 0 0; color: #808080; font-size: 8pt;">
					<font color="#808080" style="font-size: 11pt;"><b><?PHP echo $_POST['Name']; ?></b></font><font color="#808080">&nbsp;<?PHP echo $_POST['Qualifications']; ?></font>
					<font color="#3fb5e6" style="text-transform: uppercase;"><B>
						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						<?php IF ($_POST['division'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['division']; 
							}
							else
							{
							echo "<br>";
							}
						?>
					</B></font>	
					<br><br>
						<font color="#3fb5e6"><B>T </B></font>	
						<?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><font color='#3fb5e6'><B>M </B></font> ";
							echo $_POST['mobile'];
							}
						?>							
					<br><font color="#3fb5e6"><B>E </B></font>
						<?PHP echo $_POST['email']; ?>
				</div>
				<div style="Clear:both; padding: 8px 0 3px 0; color: #808080; font-size: 8pt;">
71 Fenchurch Street, London EC3M 4BS</div><div style="Clear:both; padding: 2px 0 0 0; color: #808080; font-size: 8pt;">www.rkharrison.com</div>
		</div>
				<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; height: 240px; background: #182a54; padding: 100px 25px 25px 25px; font-size: 7pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000;" id="table51" >
							<b><font size="4"><p style="letter-spacing: 5px;">EXPECT MORE</p></font><br><br></b><br><br>
							<img border="0" src="img/rkhpwback.jpg" align="left" hspace="0" width="300">
						
							<br><br>
		</div>
	<?php
	}
	?>
	
	<?php IF ($_POST['company'] == "RKH Specialty (Generic)") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #FFFFFF; padding: 25px; font-size: 8.5pt; color: #333333; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/meclogo2.jpg" align="left" hspace="0" width="300">
					</div>
				
				<div style="Clear:both; padding: 35px 0 0 0; color: #000000; font-size: 8pt;">
					<font color="#dd0330" style="font-size: 9pt;"><b><?PHP echo $_POST['Name']; ?></b></font><font color="#000000">&nbsp;<?PHP echo $_POST['Qualifications']; ?></font>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						<?php IF ($_POST['division'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['division']; 
							}
							else
							{
							echo "<br>";
							}
						?>
						

					<br><br>
						<font color="#dd0330"><B>d</B></font>	
						<?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><font color='#dd0330'><B>m</B></font> ";
							echo $_POST['mobile'];
							}
						?>							
					<br><font color="#dd0330"><B>e</B></font>
						<?PHP echo $_POST['email']; ?>
				</div>
		</div>
				<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; height: 240px; background: #dd0330; padding: 50px 25px 25px 25px; font-size: 7pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000;" id="table51" >
							
							<img border="0" src="img/mecback.png" align="left" hspace="0" width="300">
						
							<br><br>
		</div>
	<?php
	}
	?>
	
	
	
		<?php IF ($_POST['company'] == "RKH Specialty Marine Energy Construction") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #FFFFFF; padding: 25px; font-size: 8.5pt; color: #333333; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/meclogo.png" align="left" hspace="0" width="300">
					</div>
				
				<div style="Clear:both; padding: 35px 0 0 0; color: #000000; font-size: 8pt;">
					<font color="#dd0330" style="font-size: 9pt;"><b><?PHP echo $_POST['Name']; ?></b></font><font color="#000000">&nbsp;<?PHP echo $_POST['Qualifications']; ?></font>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						<?php IF ($_POST['division'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['division']; 
							}
							else
							{
							echo "<br>";
							}
						?>
						

					<br><br>
						<font color="#dd0330"><B>d</B></font>	
						<?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><font color='#dd0330'><B>m</B></font> ";
							echo $_POST['mobile'];
							}
						?>							
					<br><font color="#dd0330"><B>e</B></font>
						<?PHP echo $_POST['email']; ?>
				</div>
		</div>
				<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; height: 240px; background: #dd0330; padding: 50px 25px 25px 25px; font-size: 7pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000;" id="table51" >
							
							<img border="0" src="img/mecback.png" align="left" hspace="0" width="300">
						
							<br><br>
		</div>
	<?php
	}
	?>
	
	
	<?php IF ($_POST['company'] == "RKH Specialty Property Casualty") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #FFFFFF; padding: 25px; font-size: 8.5pt; color: #333333; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/pclogo.png" align="left" hspace="0" width="300">
					</div>
				
				<div style="Clear:both; padding: 35px 0 0 0; color: #000000; font-size: 8pt;">
					<font color="#dd0330" style="font-size: 9pt;"><b><?PHP echo $_POST['Name']; ?></b></font><font color="#000000">&nbsp;<?PHP echo $_POST['Qualifications']; ?></font>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						<?php IF ($_POST['division'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['division']; 
							}
							else
							{
							echo "<br>";
							}
						?>
						

					<br><br>
						<font color="#dd0330"><B>d</B></font>	
						<?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><font color='#dd0330'><B>m</B></font> ";
							echo $_POST['mobile'];
							}
						?>							
					<br><font color="#dd0330"><B>e</B></font>
						<?PHP echo $_POST['email']; ?>
				</div>
		</div>
				<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; height: 240px; background: #dd0330; padding: 50px 25px 25px 25px; font-size: 7pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000;" id="table51" >
							
							<img border="0" src="img/mecback.png" align="left" hspace="0" width="300">
						
							<br><br>
		</div>
	<?php
	}
	?>
	
	
	<?php IF ($_POST['company'] == "RKH Specialty Financial Risks") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #FFFFFF; padding: 25px; font-size: 8.5pt; color: #333333; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/frlogo.png" align="left" hspace="0" width="300">
					</div>
				
				<div style="Clear:both; padding: 35px 0 0 0; color: #000000; font-size: 8pt;">
					<font color="#dd0330" style="font-size: 9pt;"><b><?PHP echo $_POST['Name']; ?></b></font><font color="#000000">&nbsp;<?PHP echo $_POST['Qualifications']; ?></font>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						<?php IF ($_POST['division'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['division']; 
							}
							else
							{
							echo "<br>";
							}
						?>
						

					<br><br>
						<font color="#dd0330"><B>d</B></font>	
						<?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><font color='#dd0330'><B>m</B></font> ";
							echo $_POST['mobile'];
							}
						?>							
					<br><font color="#dd0330"><B>e</B></font>
						<?PHP echo $_POST['email']; ?>
				</div>
		</div>
				<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; height: 240px; background: #dd0330; padding: 50px 25px 25px 25px; font-size: 7pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000;" id="table51" >
							
							<img border="0" src="img/mecback.png" align="left" hspace="0" width="300">
						
							<br><br>
		</div>
	<?php
	}
	?>
	
	<?php IF ($_POST['company'] == "RKH Specialty Risk Solutions") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #FFFFFF; padding: 25px; font-size: 8.5pt; color: #333333; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/rslogo.png" align="left" hspace="0" width="300">
					</div>
				
				<div style="Clear:both; padding: 35px 0 0 0; color: #000000; font-size: 8pt;">
					<font color="#dd0330" style="font-size: 9pt;"><b><?PHP echo $_POST['Name']; ?></b></font><font color="#000000">&nbsp;<?PHP echo $_POST['Qualifications']; ?></font>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						<?php IF ($_POST['division'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['division']; 
							}
							else
							{
							echo "<br>";
							}
						?>
						

					<br><br>
						<font color="#dd0330"><B>d</B></font>	
						<?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><font color='#dd0330'><B>m</B></font> ";
							echo $_POST['mobile'];
							}
						?>							
					<br><font color="#dd0330"><B>e</B></font>
						<?PHP echo $_POST['email']; ?>
				</div>
		</div>
				<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; height: 240px; background: #dd0330; padding: 50px 25px 25px 25px; font-size: 7pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000;" id="table51" >
							
							<img border="0" src="img/mecback.png" align="left" hspace="0" width="300">
						
							<br><br>
		</div>
	<?php
	}
	?>
	
	
	
	
	<?php IF ($_POST['company'] == "RKH Reinsurance") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #FFFFFF; padding: 25px; font-size: 8.5pt; color: #333333; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/reinlogo2.jpg" align="left" hspace="0" width="300">
					</div>
				
				<div style="Clear:both; padding: 25px 0 0 0; color: #000000; font-size: 8pt;">
					<font color="#000000" style="font-size: 9pt;"><b><?PHP echo $_POST['Name']; ?></b>&nbsp;<?PHP echo $_POST['Qualifications']; ?></font>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						<?php IF ($_POST['division'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['division']; 
							}
							else
							{
							echo "<br>";
							}
						?>
						

					<br><br>
						<font color="#000000"><B>d</B></font>	
						<?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><font color='#000000'><B>m</B></font> ";
							echo $_POST['mobile'];
							}
						?>							
					<br><font color="#000000"><B>e</B></font>
						<?PHP echo $_POST['email']; ?>
				</div>
		</div>
				<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; height: 240px; background: #231f20; padding: 50px 25px 25px 25px; font-size: 7pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000;" id="table51" >
							
							<img border="0" src="img/reinback2.jpg" align="left" hspace="0" width="300">
						
							<br><br>
		</div>
	<?php
	}
	?>
	
	
			
	<?php IF ($_POST['company'] == "GCS") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #FFFFFF; padding: 25px; font-size: 8.5pt; color: #333333; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/GCSlogo.png" align="right" hspace="0" width="160">
					</div>
				
				<div style="Clear:both; padding: 35px 0 0 0; color: #808080; font-size: 8pt;">
					<font color="#06b28e" style="font-size: 9pt;"><b><?PHP echo $_POST['Name']; ?></b></font><font color="#808080">&nbsp;<?PHP echo $_POST['Qualifications']; ?></font>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						<?php IF ($_POST['division'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['division']; 
							}
							else
							{
							echo "<br>";
							}
						?>
						

					<br><br>
						<font color="#06b28e"><B>D</B></font>	
						<?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><font color='#06b28e'><B>M</B></font> ";
							echo $_POST['mobile'];
							}
						?>							
					<br><font color="#06b28e"><B>E</B></font>
						<?PHP echo $_POST['email']; ?>
				</div>
		</div>
				<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; height: 240px; background: #06b28e; padding: 0; font-size: 7pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000;" id="table51" >
							
							<img border="0" src="img/GCSback.png" align="left" hspace="0" width="360">
						
							<br><br>
		</div>
	<?php
	}
	?>
	
	
	
	<?php IF ($_POST['company'] == "NHF") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #FFFFFF; padding: 25px; font-size: 8.5pt; color: #333333; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/NHFlogo.png" align="right" hspace="0" width="120">
					</div>
				
				<div style="Clear:both; padding: 15px 0 0 0; color: #000000; font-size: 8pt;">
					<font color="#ec068d" style="font-size: 9pt;"><b><?PHP echo $_POST['Name']; ?></b></font><font color="#000000">&nbsp;<?PHP echo $_POST['Qualifications']; ?></font>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						<?php IF ($_POST['division'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['division']; 
							}
							else
							{
							echo "<br>";
							}
						?>
						

					<br><br>
						<font color="#ec068d"><B>D</B></font>	
						<?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><font color='#ec068d'><B>M</B></font> ";
							echo $_POST['mobile'];
							}
						?>							
					<br><font color="#ec068d"><B>E</B></font>
						<?PHP echo $_POST['email']; ?>
				</div>
		</div>
				<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; height: 240px; background: #ec068d; padding: 50px 25px 25px 25px; font-size: 7pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000;" id="table51" >
							
							<img border="0" src="img/nhfback.png" align="left" hspace="0" width="240">
						
							<br><br>
		</div>
	<?php
	}
	?>
	
	
	<?php IF ($_POST['company'] == "BPIF") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; height: 240px; background: #FFFFFF; padding: 0px; font-size: 7pt; color: #333333; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%; padding: 25px 0 30px 0;">
					<img border="0" src="img/BPIFlogo.png" align="left" hspace="0" width="250">
					</div>
				
				<div style="Clear:both; padding: 35px 0 0 150px; color: #000000; font-size: 7pt;">
					<font color="#c71d22" style="font-size: 8pt;"><b><?PHP echo $_POST['Name']; ?></b></font><font color="#000000">&nbsp;<?PHP echo $_POST['Qualifications']; ?></font>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						<?php IF ($_POST['division'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['division']; 
							}
							else
							{
							echo "<br>";
							}
						?>
						

					<br><br>
						<font color="#c71d22"><B>D</B></font>	
						<?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><font color='#c71d22'><B>M</B></font> ";
							echo $_POST['mobile'];
							}
						?>							
					<br><font color="#c71d22"><B>E</B></font>
						<?PHP echo $_POST['email']; ?>
						<br><br>
						<font color="#c71d22" style="font-size: 7pt;"><b>Trust us to protect your business</b></font>
				</div>
		</div>
				<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; height: 240px; background: #c71d22; padding: 0; font-size: 7pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000;" id="table51" >
							
							<img border="0" src="img/BPIFback.png" align="left" hspace="0" width="360">
						
							<br><br>
		</div>
	<?php
	}
	?>
	
	
	
	<?php IF ($_POST['company'] == "Bowood Insurance Brokers") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #FFFFFF; padding: 25px; font-size: 8.5pt; color: #333333; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/bowoodlogo.png" align="left" hspace="0" width="130">
					</div>
				
				<div style="Clear:both; padding: 35px 0 0 0; color: #000000; font-size: 8pt;">
					<font color="#1b5630" style="font-size: 9pt;"><b><?PHP echo $_POST['Name']; ?></b>&nbsp;<?PHP echo $_POST['Qualifications']; ?></font>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						
						

					<br><br>
						<font color="#1b5630"><B>d</B></font>	
						<?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><font color='#1b5630'><B>m</B></font> ";
							echo $_POST['mobile'];
							}
						?>							
					<br><font color="#1b5630"><B>e</B></font>
						<?PHP echo $_POST['email']; ?>
				</div>
		</div>
				<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; height: 240px; background: #1b5630; padding: 50px 25px 25px 25px; font-size: 7pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000;" id="table51" >
							
							<img border="0" src="img/bowoodback.png" align="left" hspace="0" width="300">
						
							<br><br>
		</div>
	<?php
	}
	?>
	
	
		
	<?php IF ($_POST['company'] == "Dual Private Client") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #231f20; padding: 0 25px 25px 25px; font-size: 8.5pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/dualpc.png" align="left" style="padding: 30px 0 30px 0;">
					</div>
				
				<div style="Clear:both; padding: 0 0 0 0;">
					<b><?PHP echo $_POST['Name']; ?></b>&nbsp;<?PHP echo $_POST['Qualifications']; ?>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						<?php IF ($_POST['division'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['division']; 
							}
						?>

					<br><br>
						<B>D </B><?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><B>M </B>";
							echo $_POST['mobile'];
							}
						?>							
					<br><B>E </B>
						<?PHP echo $_POST['email']; ?>
				</div>
		</div>
		<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; background: #231f20; padding: 0 25px 25px 25px; font-size: 8.5pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<p style="text-align: center">
					<img border="0" src="img/dualpcbg.png" align="center" style="padding: 30px 0 10px 0;"><br>
					<b>&nbsp;DUAL Underwriting Limited<br>
					155 Fenchurch Street, London EC3M 6AL</b><br>
					<br>
					+44 (0)20 7397 4404<br>
					aqua@dualunderwriting.com<br>
					www.dualprivateclient.com<br>
					</div>
		</div>
	<?php
	}
	?>
	
	
	<?php IF ($_POST['company'] == "Dual Private Client Aurum") { ?>			


			<p class="cardlabel">Front of Card</p>

			<div>					
				<div Style="width: 360px; background: #231f20; padding: 0 25px 25px 25px; font-size: 8.5pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<img border="0" src="img/dualpc.png" align="left" style="padding: 30px 0 30px 0;">
					</div>
				
				<div style="Clear:both; padding: 0 0 0 0;">
					<b><?PHP echo $_POST['Name']; ?></b>&nbsp;<?PHP echo $_POST['Qualifications']; ?>

						<?php IF ($_POST['jobtitle'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['jobtitle']; 
							}
						?>
						<?php IF ($_POST['division'] !== "") 
							{ 
							echo "<br>";
							echo $_POST['division']; 
							}
						?>

					<br><br>
						<B>D </B><?PHP echo $_POST['ddi']; ?>
						<?php IF ($_POST['mobile'] !== "") 
							{ 
							echo "<BR><B>M </B>";
							echo $_POST['mobile'];
							}
						?>							
					<br><B>E </B>
						<?PHP echo $_POST['email']; ?>
				</div>
		</div>
		<p class="cardlabel">Back of Card</p>
		<div Style="width: 360px; background: #86754e; padding: 0 25px 25px 25px; font-size: 8.5pt; color: #FFFFFF; font-family: Arial; border: 1pt solid #000000; text-align: left;" id="table51" >
					<div style="width: 100%;">
					<p style="text-align: center">
					<img border="0" src="img/dualpcbgaurum.png" align="center" style="padding: 30px 0 10px 0;"><br>
					<b>&nbsp;DUAL Underwriting Limited<br>
					155 Fenchurch Street, London EC3M 6AL</b><br>
					<br>
					+44 (0)20 7397 4404<br>
					aqua@dualunderwriting.com<br>
					www.dualprivateclient.com<br>
					</div>
		</div>
	<?php
	}
	?>

	
				
				<form method="POST" action="bizcardadd.php">
					
							<input type="hidden" name="location" size="1" value="<?PHP echo $_POST['location']; ?>">
							<input type="hidden" name="company" size="1" value="<?PHP echo $_POST['company']; ?>">
							<input type="hidden" name="cardname" size="1" value="<?PHP echo $_POST['Name']; ?>">
							<input type="hidden" name="Qualifications" size="1" value="<?PHP echo $_POST['Qualifications']; ?>">
							<input type="hidden" name="jobtitle" size="1" value="<?PHP echo $_POST['jobtitle']; ?>">
							<input type="hidden" name="division" size="1" value="<?PHP echo $_POST['division']; ?>">
							<input type="hidden" name="ddi" size="1" value="<?PHP echo $_POST['ddi']; ?>">
							<input type="hidden" name="mobile" size="1" value="<?PHP echo $_POST['mobile']; ?>">
							<input type="hidden" name="email" size="1" value="<?PHP echo $_POST['email']; ?>">
							<input type="hidden" NAME="AddedBy" SIZE="1" VALUE="<?PHP echo $_SERVER ['REMOTE_USER']; ?>">
							<input type="hidden" name="addnotes" size="1" value="<?PHP echo $_POST['addnotes']; ?>">
				<BR><BR>
				
				<button <?php IF ($_POST['location'] == "" OR $_POST['Name'] == "" OR $_POST['ddi'] == "" OR $_POST['email'] == "" OR $_POST['company'] == "") { echo "disabled";}	?> type="submit" class="btn btn-primary btn-lg btn-block">Request Card</button>
				<BR>Each order represents 2 boxes which equates to 250 cards.<BR>
				If you require more than this, simply add another identical order to your basket.
				</form>			
		  
	  </div>
	  </div>
</div>
</div> <!-- /.container -->
</body>
</html>