<?php
                ini_set('display_errors', 'On');
                error_reporting(E_ALL | E_STRICT);
                
                $dbfilename = $_SERVER["DOCUMENT_ROOT"] . './facilities/legalcopy/facilitiescopy.mdb';
                $dbuser = "";
                $dbpassword = "";
                if (!file_exists($dbfilename)) {
                                die("Could not find database file.");
                }
                $conn = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)};Dbq=$dbfilename", $dbuser, $dbpassword);

?>
	
<?php
$Your_Name = isset($_POST["Your_Name"]) ? $_POST["Your_Name"] : '';
$Short_Desc = isset($_POST["Short_Desc"]) ? $_POST["Short_Desc"] : '';

$Full_Desc = isset($_POST["Full_Desc"]) ? $_POST["Full_Desc"] : '';
$Location = isset($_POST["Location"]) ? $_POST["Location"] : '';
$floor = isset($_POST["floor"]) ? $_POST["floor"] : '';
$owner = isset($_POST["owner"]) ? $_POST["owner"] : '';
$Priority = isset($_POST["Priority"]) ? $_POST["Priority"] : '';
$Cost_Centre = isset($_POST["Cost_Centre"]) ? $_POST["Cost_Centre"] : '';
$Cost_Amount = isset($_POST["Cost_Amount"]) ? $_POST["Cost_Amount"] : '';
$Contractor = isset($_POST["Contractor"]) ? $_POST["Contractor"] : '';
$Follow_up_date = isset($_POST["Follow_up_date"]) ? $_POST["Follow_up_date"] : '';
$NewAction = isset($_POST["NewAction"]) ? $_POST["NewAction"] : '';


$adminuser = null;
$whoisit = str_replace("'", "",  $_SERVER['REMOTE_USER']);
$whoisit3 = substr(strstr($whoisit, '\\'), strlen('\\'));
$sql2  = "SELECT admin FROM adminusers WHERE ntlogin = '$whoisit'";
$result2 = $conn->query($sql2); 
while($row2 = $result2->fetch()) 





{
$adminuser = $row2["admin"];

}



?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Facilities helpdesk</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/datepicker.css">
<!-- <link href="css/bootstrap-theme.min.css" rel="stylesheet"> -->
<link href="css/styles.css" rel="stylesheet">
<script type="text/javascript" src="//use.typekit.net/vjr8xic.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/bootstrap.js"></script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>

<style>

.p {padding: 140px;}

</style>

</head>

<body>

























<div class="container" style="width: 100%; height: 100%">
	<h1><img border="0" src="faclogo.jpg"  style="padding-left: 20px"></h1>
	<div class="panel panel-default panel-preview" style="width:35%; float: left; min-width: 300px; max-width: 360px; margin-right: 20px; margin-left: 20px;">
      
	  <div class="panel-heading">
        <h3 class="panel-title">New Helpdesk Request</h3>
      </div>
	  <form method="POST" action="insert.php">
	  <div class="panel-body">
			<div>
  


  
<p><B>Category of request</b></p>
		  <div class="dropdown">  
  <select id="Short_Desc" name="Short_Desc" class="form-control">
	  
	  
	<option selected>Select from list</option>
<option value="Archive System">Archive System</option>
<option value="Cleaning">Cleaning</option>
<option value="Climate Control">Climate Control</option>
<option value="Couriers/Post">Couriers/Post</option>
<option value="Decoration">Decoration</option>
<option value="Electrical">Electrical </option>
<option value="External Areas">External Areas</option>
<option value="Fixtures, Fittings &amp; Equipment">Fixtures, Fittings & Equipment</option>
<option value="Health & Safety">Health & Safety</option>
<option value="Lifts">Lifts</option>
<option value="Office Consumables">Office Consumables</option>
<option value="Plumbing">Plumbing</option>
<option value="Print Device - Other">Print Device - Other</option>
<option value="Print Device - Ricoh">Print Device - Ricoh</option>
<option value="Print Device - Xerox">Print Device - Xerox</option>
<option value="Security">Security</option>
<option value="Staff Relocation">Staff Relocation</option>
<option value="Stationery">Stationery</option>
<option value="Vending">Vending</option>  
	  
</select>
</div>
	  
	<BR> 
	  
	  

<p><B>Brief Description of Request</b></p>
		<textarea id="Full_Desc" name="Full_Desc" class="form-control" rows="2">		</textarea>



<BR>




<p><B>Building</b></p>
		  <div class="dropdown">  
  <select id="Building" name="Building" class="form-control">
	  
	<option value="Bedford">Bedford Woodlands</option>
<option value="BedfordLakeview">Bedford Lakeview</option>
				<option value="Bermuda">Bermuda</option>
				<option value="Birmingham">Birmingham</option>
<option value="Brighton">Brighton</option>
<option value="Cardiff">Cardiff</option>
<option value="Dublin">Dublin</option>
				<option value="Durrington">Durrington</option>
				<option value="Glasgow">Glasgow</option>
				<option value="Leeds">Leeds</option>
<option value="Liverpool">Liverpool</option>
				<option value="London Whittington">London Whitington</option>
				<option value="London 71 Fenchurch St">London 71 Fenchurch St</option>
				<option value="London 155 Fenchurch St">London 155 Fenchurch St</option>
				<option value="London Eastcheap">London Eastcheap</option>
				<option value="London 107 Leadenhall">London 107 Leadenhall</option>
<option value="Manchester">Manchester</option>
<option value="Norwich">Norwich</option>
				<option value="Peterborough">Peterborough</option>
				<option value="Poole">Poole</option>
				<option value="Redhill">Redhill</option>
				<option value="Richmond">Richmond</option>
<option value="Wakefield">Wakefield</option>	  
 	  
</select>
</div>


<BR>

	
<p><b>Location</b> (e.g. First Floor Landing)</p>
		<input type="Location" id="Location" name="Location" class="form-control">	

<BR>
	
		<input type="submit" value="Submit" class="btn btn-success btn-md">
	
	
	  </div>
	 </div>
	 </form>
	 </div> 









<div class="panel" style="width:70%; float: left; margin: 0 0 0 0; min-width: 400px; font-size: 9pt">






<ul class="nav nav-tabs" style="font-size: 14pt;">
  <li><a href="index.php">Outstanding</a></li>
  <li><a href="index2.php">Completed</a></li>
  <li class="active" style="color: #f2f2f2"><a href="search.php">Search</a></li>

</ul>
<div class="panel panel-default panel-preview" style="width:100%; float: left; margin: 0 0 0 0; min-width: 400px; font-size: 9pt; border-top: 0">
			<div class="panel-body" style="background: #f2f2f2;" >
			
			
			<form method="post" action="" class="form-horizontal" width="300px">
  <div class="form-group" style="text-align: left;">
    <label class="control-label col-sm-1">Requestor</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="Your_Name" name="Your_Name" value="Select">
    </div>
  </div>

  
<div class="form-group">
		   
		     <label class="control-label col-sm-1">Category</label>
  <div class="col-sm-5">
  <select id="Short_Desc" name="Short_Desc" class="form-control">	  
	  
	<option value="Select" selected>Select</option>
<option value="Archive System">Archive System</option>
<option value="Cleaning">Cleaning</option>
<option value="Climate Control">Climate Control</option>
<option value="Couriers/Post">Couriers/Post</option>
<option value="Decoration">Decoration</option>
<option value="Electrical">Electrical </option>
<option value="External Areas">External Areas</option>
<option value="Fixtures, Fittings &amp; Equipment">Fixtures, Fittings & Equipment</option>
<option value="Health & Safety">Health & Safety</option>
<option value="Lifts">Lifts</option>
<option value="Office Consumables">Office Consumables</option>
<option value="Plumbing">Plumbing</option>
<option value="Print Device - Other">Print Device - Other</option>
<option value="Print Device - Ricoh">Print Device - Ricoh</option>
<option value="Print Device - Xerox">Print Device - Xerox</option>
<option value="Security">Security</option>
<option value="Staff Relocation">Staff Relocation</option>
<option value="Stationery">Stationery</option>
<option value="Vending">Vending</option>  
	  
</select>
</div>  
  </div> 
  
  
  
  
 <div class="form-group">  
<label class="control-label col-sm-1" >Building</label>
		   <div class="col-sm-5"> <div class="dropdown">  
  <select id="Location" name="Location" class="form-control">
	<option value="Select" selected>Select</option>	  
	<option value="Bedford">Bedford Woodlands</option>
<option value="BedfordLakeview">Bedford Lakeview</option>
				<option value="Bermuda">Bermuda</option>
				<option value="Birmingham">Birmingham</option>
<option value="Brighton">Brighton</option>
<option value="Cardiff">Cardiff</option>
<option value="Dublin">Dublin</option>
				<option value="Durrington">Durrington</option>
				<option value="Glasgow">Glasgow</option>
				<option value="Leeds">Leeds</option>
<option value="Liverpool">Liverpool</option>
				<option value="London Whittington">London Whitington</option>
				<option value="London 71 Fenchurch St">London 71 Fenchurch St</option>
				<option value="London 155 Fenchurch St">London 155 Fenchurch St</option>
				<option value="London Eastcheap">London Eastcheap</option>
				<option value="London 107 Leadenhall">London 107 Leadenhall</option>
<option value="Manchester">Manchester</option>
<option value="Norwich">Norwich</option>
				<option value="Peterborough">Peterborough</option>
				<option value="Poole">Poole</option>
				<option value="Redhill">Redhill</option>
				<option value="Richmond">Richmond</option>
<option value="Wakefield">Wakefield</option>	  
 	  
</select>
</div></div>
</div>  
  
  
  
  
    <div class="form-group">
    <label class="control-label col-sm-1">Location</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="floor" name="floor" value="Select">
    </div>
  </div>

 

  
  
 <div class="form-group">  
<label class="control-label col-sm-1">Allocated</label>
		   <div class="col-sm-5"> 
		  <div class="dropdown">  
  <select id="owner" name="owner" class="form-control">

<option value="Select" selected>Select</option>	
<option value="LBernard">LBernard</option>
<option value="ABrabin">ABrabin</option>
<option value="Lisa.Deehan">Lisa.Deehan</option>
<option value="FFarr">FFarr</option>
<option value="Richard.Hardwick">Richard.Hardwick</option>
<option value="CHolness">CHolness</option>
<option value="Peter.Nevill">Peter.Nevill</option>
<option value="LNewman">LNewman</option>
<option value="SOConnell">SOConnell</option>
<option value="Nicola.ODell">Nicola.ODell</option>
<option value="Oliver.Smith">Oliver.Smith</option>
<option value="RStukins">RStukins</option>
<option value="Nicola.Warner">Nicola.Warner</option>
<option value="Mark.Bichener">Mark.Bichener</option>
	  
 	  
</select>
</div>	
 </div>
 </div>
  
  
 
 <div class="form-group">  
<label class="control-label col-sm-1">Contractor</label>
		   <div class="col-sm-5"> 
		  <div class="dropdown">  
  <select id="Contractor" name="Contractor" class="form-control">

  <option value="Select" selected>Select</option>	
<option value="NOT APPLICABLE">NOT APPLICABLE</option>
<OPTION VALUE="ACE SECURITY">ACE SECURITY</OPTION>
<OPTION VALUE="ADT FIRE AND SECURITY">ADT FIRE AND SECURITY</OPTION>
<OPTION VALUE="ADT">ADT</OPTION>
<OPTION VALUE="ARC MONITORING CENTRE">ARC MONITORING CENTRE</OPTION>
<OPTION VALUE="BAXTER STOREY">BAXTER STOREY</OPTION>
<OPTION VALUE="BIRKIN">BIRKIN</OPTION>
<OPTION VALUE="BOBTAIL">BOBTAIL</OPTION>
<OPTION VALUE="BUG-BUSTERS IT HYGIENE">BUG-BUSTERS IT HYGIENE</OPTION>
<OPTION VALUE="CARDO">CARDO</OPTION>
<OPTION VALUE="CHUBB">CHUBB</OPTION>
<OPTION VALUE="CITY FIRE">CITY FIRE</OPTION>
<OPTION VALUE="CONTROL GROUP">CONTROL GROUP</OPTION>
<OPTION VALUE="CROWN LIFTS">CROWN LIFTS</OPTION>
<OPTION VALUE="CRYSTAL">CRYSTAL</OPTION>
<OPTION VALUE="DSD WASTE">DSD WASTE</OPTION>
<OPTION VALUE="ECOURIER">ECOURIER</OPTION>
<OPTION VALUE="EDDISONS">EDDISONS</OPTION>
<OPTION VALUE="EDEN">EDEN</OPTION>
<OPTION VALUE="EUROPEAN">EUROPEAN</OPTION>
<OPTION VALUE="GC GLAZING">GC GLAZING</OPTION>
<OPTION VALUE="GUARDIAN ENVIRONMENTAL SERVICES">GUARDIAN ENVIRONMENTAL SERVICES</OPTION>
<OPTION VALUE="HAMMONDS INTERIORS LTD">HAMMONDS INTERIORS LTD</OPTION>
<OPTION VALUE="HIGHWAYS AGENCY">HIGHWAYS AGENCY</OPTION>
<OPTION VALUE="HRD SECURITY">HRD SECURITY</OPTION>
<OPTION VALUE="IMAGE DIRECT - OFFICE STATIONERY">IMAGE DIRECT - OFFICE STATIONERY</OPTION>
<OPTION VALUE="ISS SECURITY">ISS SECURITY</OPTION>
<OPTION VALUE="JAX DESIGN">JAX DESIGN</OPTION>
<OPTION VALUE="JUNGLEWORLD">JUNGLEWORLD</OPTION>
<OPTION VALUE="LANDMARK LIFTS">LANDMARK LIFTS</OPTION>
<OPTION VALUE="LEWIS & GRAVES">LEWIS & GRAVES</OPTION>
<OPTION VALUE="LLOYDS REGISTER BUILDING MANAGEMENT">LLOYDS REGISTER BUILDING MANAGEMENT</OPTION>
<OPTION VALUE="NEOPOST">NEOPOST</OPTION>
<OPTION VALUE="ONE TO ONE">ONE TO ONE</OPTION>
<OPTION VALUE="OPTIMUM">OPTIMUM</OPTION>
<OPTION VALUE="ORION SECURITY">ORION SECURITY</OPTION>
<OPTION VALUE="OSG ACCESS CONTROL">OSG ACCESS CONTROL</OPTION>
<OPTION VALUE="PAGE DECORATORS">PAGE DECORATORS</OPTION>
<OPTION VALUE="PALMAC">PALMAC</OPTION>
<OPTION VALUE="R+B DOORS">R+B DOORS</OPTION>
<OPTION VALUE="RESTORE RECORD MANAGEMENT">RESTORE RECORD MANAGEMENT</OPTION>
<OPTION VALUE="ROYAL MAIL">ROYAL MAIL</OPTION>
<OPTION VALUE="SELECT DRINKS">SELECT DRINKS</OPTION>
<OPTION VALUE="SHIELD GUARDING">SHIELD GUARDING</OPTION>
<OPTION VALUE="SHRED FIRST">SHRED FIRST</OPTION>
<OPTION VALUE="SMARTS SECURITY">SMARTS SECURITY</OPTION>
<OPTION VALUE="SWIFT LIFTS">SWIFT LISTS</OPTION>
<OPTION VALUE="SYNERGY">SYNERGY</OPTION>
<OPTION VALUE="THE FURNITURE PRACTICE">THE FURNITURE PRACTICE</OPTION>
<OPTION VALUE="TRANSWORLD COURIERS">TRANSWORLD COURIERS</OPTION>
<OPTION VALUE="VITESSE">VITESSE</OPTION>
<OPTION VALUE="WILLIS NEWS ">WILLIS NEWS </OPTION>

</select>
</div>	 
 </div>
</div> 
  
  
    <div class="form-group">
    <label class="control-label col-sm-1">Cost Centre</label>
    <div class="col-sm-5">
      <input type="Cost_Centre" class="form-control" id="Cost_Centre" value="Select">
    </div>
  </div>
  
  
 
  
  <div class="form-group"> 
    <div class="col-sm-offset-1 col-sm-1">
      <button type="submit" class="btn btn-default">Search</button>
    </div>
  </div>
</form>
			
			
			

<table>
<tr>
<th>ID</th>
<th>Requestor</th>
<th>Category</th>
<th>Full Description</th>
<th>Office</th>
<th>Location</th>
<th>Reported</th>
<th>Allocated</th>
<th>Priority</th>
<th>Status</th>
</tr>



<?php

if ($Location !== "Select") {$string = "AND Location ='$Location'";} ELSE { $string =''; }

if ($Short_Desc !== "Select") {$string2 = "AND Short_Desc ='$Short_Desc'";} ELSE { $string2 =''; }

if ($Your_Name !== "Select") {$string3 = "AND Your_Name LIKE '%$Your_Name%'";} ELSE { $string3 =''; }

if ($floor !== "Select") {$string4 = "AND floor = '$floor'";} ELSE { $string4 =''; }

if ($owner !== "Select") {$string5 = "AND owner = '$owner'";} ELSE { $string5 =''; }

if ($Contractor !== "Select") {$string6 = "AND Contractor = '$Contractor'";} ELSE { $string6 =''; }

if ($Cost_Centre !== "Select") {$string7 = "AND Cost_Centre = '$Cost_Centre'";} ELSE { $string7 =''; }



IF ($adminuser == "Y") {
$sql3  = "SELECT Top 200 ID, Your_Name, Short_Desc, Location, floor, Shortdate, owner, Priority, Entry_Text, Full_Desc, Contractor, Cost_Centre, colorchk FROM QuerySearch WHERE ID > 1 " . $string . $string2 . $string3 . $string4 . $string5 . $string6 . $string7 . "ORDER BY ID DESC";


} 


$result3 = $conn->query($sql3); 
	while($row3 = $result3->fetch()) {
																	
                                                                                         echo "<form method='POST' action='edit.php'>";                                                     
                                                                                                echo "<tr>";
																								echo "<td><input style ='background: ". $row3['colorchk'] ." ' type='submit' value='" . $row3['ID'] . "'><input type='hidden' name='ID' ID='ID' value='" . $row3['ID'] . "'></td>";
																								echo "<td>" . $row3['Your_Name'] . "</td>";
																								echo "<td>" . $row3['Short_Desc'] . "</td>";
																								echo "<td>" . $row3['Full_Desc'] . "</td>";
																								echo "<td>" . $row3['Location'] . "</td>";
																								echo "<td>" . $row3['floor'] . "</td>";
																								echo "<td>" . $row3['Shortdate'] . "</td>";
																								echo "<td>" . $row3['owner'] . "</td>";
																								echo "<td>" . $row3['Priority'] . "</td>";
																								echo "<td>" . $row3['Entry_Text'] . "</td>";
																								echo "</tr>";                     
                                                                                         echo "</form>";                                             
																	     
                                                                }
                                                                echo "</table>";                         
	  	

	  
?>







			</div>
</div>

</div>
















	 
	
	
	
</div>



<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>

   <script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('#ContractDate').datepicker({
                    format: "dd/mm/yyyy"
                });  
				$('#ComDate').datepicker({
                    format: "dd/mm/yyyy"
                }); 
				$('#ExpiryDate').datepicker({
                    format: "dd/mm/yyyy"
                });
            });
        </script>


</body>
</html>