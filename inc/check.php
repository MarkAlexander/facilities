<?php
require_once( '/intranet/lib/sanitize.php' );

$input = $inputErr = array(
  "insured" => "",
  "territories" => "",
  "cob" => "",
  "hrlob" => "",
  "ownership" => ""
);

if ( $_SERVER[ 'REQUEST_METHOD' ] == 'POST' ) {
	empty( $_POST[ 'insured' ] ) ? $inputErr[ 'insured' ] = "Name of the insured is missing." :
    $input[ 'insured' ] = test_input( $_POST[ 'insured' ] );
  if ( count( $_POST[ 'territories' ] ) == 0 ) {
    $inputErr[ 'territories' ] = "No territories provided.";
  }
  else {
    foreach ( $_POST[ 'territories' ] as $territory ) {
      if ( empty( $territory ) ) {
        $inputErr[ 'territories' ] = "At least one of the territories is empty.";
      }
    }
    if ( count( $inputErr[ 'territories' ] ) == 0 ) {
      $input[ 'territories' ] = $_POST[ 'territories' ];
    }
  }
  empty( $_POST[ 'cob' ] ) ? $inputErr[ 'cob' ] = "COB not provided." :
    $input[ 'cob' ] = test_input( $_POST[ 'cob' ] );
  empty( $_POST[ 'hrlob' ] ) ? $inputErr[ 'hrlob' ] = "High Risk LOB not provided." :
    $input[ 'hrlob' ] = test_input( $_POST[ 'hrlob' ] );
  empty( $_POST[ 'ownership' ] ) ? $inputErr[ 'ownership' ] = "Ownership not provided." :
    $input[ 'ownership' ] = test_input( $_POST[ 'ownership' ] );
  
}
