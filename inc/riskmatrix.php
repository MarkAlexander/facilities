<?php
/**
 * Sanctions Checking Rating Matrix
 * 
 * Sanctions Checking Rating Matrix to identify accounts that require
 * sanctions checking of Secondary Interests. Score of 1 or more requires
 * deep dive sanctions check.
 */

// Gets the risk associated to a country by its CPI
function countryRisk( $cpi ) {
  // CPI > 6
  if ( $cpi >= 6 )
    return 0;
  // CPI 4-6
  else if ( $cpi < 6 && $cpi >= 4 )
    return 0.4;
  // CPI 2-3
  else if ( $cpi < 4 && $cpi >= 2 )
    return 0.5;
  // CPI 1 or unrated
  else if ( $cpi < 2 )
    return 0.8;
  else
    return NaN;
  //TODO: Sanctioned Territory OFAC and/or EU?
}

function getScore() {
  $totalWeight = 0;
  foreach ($_POST[ 'territoryWeights' ] as $weight) {
    $totalWeight += $weight;
  }
  
  $score = 0;
  $size = count( $_POST[ 'territories' ] );
  for ( $i = 0; $i < $size; $i++ ) {
    $score += countryRisk( $_POST[ 'territories' ][ $i ] ) * $_POST[ 'territoryWeights' ][ $i ] / $totalWeight;
  }
  
  $score += $_POST[ 'cob' ];
  $score += $_POST[ 'hrlob' ];
  $score += $_POST[ 'ownership' ];
  
  return $score;
}

/*
// Territory risk
$territory = array(
  'sanctioned'          =>  1.0,
  'cpi-gt6'             =>  0.0,
  'cpi-4to6'            =>  0.4,
  'cpi-2to3'            =>  0.5,
  'cpi-1'               =>  0.8,
);

// COB risk matrix
$cob = array(
  'indemnity'           =>  0.0,
  'directors'           =>  0.0,
  'trade-credit'        =>  0.1,
  'non-marine'          =>  0.3,
  'motor'               =>  0.3,
  'personal-accident'   =>  0.3,
  'contingency'         =>  0.3,
  'political'           =>  0.3,
  'energy'              =>  0.4,
  'marine-gte6'         =>  0.2,
  'marine-lt6'          =>  0.4,
  'specie'              =>  0.5,
  'fine-art'            =>  0.5,
);

// High Risk LOB risk matrix
$hrlob = array(
  'oil-gas-services'    =>  0.1,
  'gas'                 =>  0.2,
  'oil'                 =>  0.2,
  'oil-gas-exploration' =>  0.3,
  'construction'        =>  0.3,
  'mining'              =>  0.4,
  'military'            =>  0.8,
  'dual-use'            =>  0.5,
);

// Ownership risk matrix
$ownership = array(
  'individual'          =>  0.3,
  'priv'                =>  0.2,
  'notknown'			=>  0.2,
  'pub'                 =>  0.1,
  'state'               =>  0.0,
);
*/
