<?php
/**
 * Sanitize Library (2014-2015)
 * 
 * @author Modula
 */

 
/**
 * Sanitize data coming from unsafe sources, e.g. user forms
 */
function test_input( $data, $trim = true ) {
	if ( $trim ) {
	  $data = trim( $data );
	}
	$data = stripslashes( $data );
	$data = htmlspecialchars( $data );
	return $data;
}


/**
 * Get a sanitize string to use as a filename.
 */
function sanitize_filename( $filename ) {
  // Remove anything which isn't a word, whitespace, number
  // or any of the following caracters: -_~,;:[]().
  $filename = preg_replace( "([^\w\s\d\-_~,;:\[\]\(\).])", '', $filename );
  // Remove any runs of periods
  $filename = preg_replace( "([\.]{2,})", '', $filename );
  return $filename;
}
