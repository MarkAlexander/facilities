<!DOCTYPE html> 
<html> 
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/datepicker.css">
<link href="css/bootstrap-theme.min.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<script type="text/javascript" src="//use.typekit.net/vjr8xic.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/bootstrap.js"></script>

<?php
ini_set('display_errors', 'Off');
error_reporting(E_ALL | E_STRICT);        
$conn=mysqli_connect("localhost","root","tntsumo556","facilities");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
$Filter = "N";
?>

<style>
.p {padding: 140px;}
.oneValue {background: #e6e6e6;}
.anotherValue {background: #f2f2f2;}
</style>

</head>


<div class="container-fluid" style="color: #FFFFFF; background: #005e85;">
  <div class="navbar-header" style="padding: 7px 0;">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
	
	<a class="navbar-brand" rel="home" href="indexSQL.php" title="Facilities Help" style="width: 167px!important;">
        <img style="max-width:150px; margin-top: -5px;"
             src="img/hyplogo.png">
    </a>
	
    <a class="navbar-brand" href="index.php" style="width: 300px!important;">
    <span><font style="font-weight: 100; font-size: 34px; color: #FFFFFF; font-family: Myriad-Pro, Arial, Helvetica, Sans Serif;">Facilities Helpdesk</font></span></a>
  </div>
  <div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
      <li><a style="color: #FFFFFF" href="http://intranet1/intranet/"><span class="glyphicon glyphicon-log-out"></span> Exit</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a style="float: left; cursor: pointer; color: #FFFFFF" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-question-sign"></span> Help</a></li>
    </ul>
  </div>
</div>


<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Facilities Helpdesk</h3>
      </div>
      <div class="modal-body">
	  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet purus quis nibh facilisis volutpat. Nunc in porttitor eros. Suspendisse orci tellus, rutrum eget dui blandit, elementum fermentum felis. Proin luctus eleifend viverra.<BR><BR>
	  Donec quam ipsum, facilisis eu nulla id, aliquam congue tortor. Sed orci justo, fermentum sit amet scelerisque quis, fermentum nec lacus. Nullam lorem eros, efficitur in mi ut, mattis venenatis magna. Mauris luctus et tortor et pellentesque. Suspendisse potenti. Nunc a sodales augue.<BR><BR>
      <img src="img/lisadeehan.jpg "><BR><BR>
	  <img src="img/cliveholness.jpg "><BR><BR>
	  <img src="img/richardhardwick.jpg "><BR><BR>
	  <img src="img/peternevill.jpg "><BR><BR>
	  For a full list of Facilities contacts please view the <a href="http://intranet1/intranet/facilitiescontacts">Facilities intranet page</a>
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>