
<BR>
<div class="panel panel-default panel-preview" style="width:35%; float: left; min-width: 300px; max-width: 360px; margin-left: 20px;">
      
	  <div class="panel-heading">
        <h3 class="panel-title" style="font-size: 12pt;">New Helpdesk Request</h3>
      </div>
	  
	  

	  
	  <form method="POST" action="insertSQL.php">
	  <div class="panel-body">
			<div>
				<div class="dropdown">  
				
				<?php
				echo '<select id="mainCat" name="mainCat" class="form-control">'; // Open your drop down box
				
					$sql2 = mysqli_query($conn,"SELECT * FROM category");
					while($row2 = mysqli_fetch_array($sql2))
					{
					   echo '<option value="'.$row2['categoryID'].'">'.$row2['categoryName'].'</option>';
					}
					
				echo '</select>';// Close your drop down box    
				?> 
				
				
					
				</div>
				<br>
					<textarea id="Full_Desc" name="Full_Desc" class="form-control" rows="3" style="color: #000000;" placeholder="Provide a short description of your request"></textarea>
					<br>
				<div class="dropdown">  
					<select id="Building" name="Building" class="form-control">
						<option selected disabled>Select office location from list</option>
						<option value="Bedford">Bedford Woodlands</option>
						<option value="BedfordLakeview">Bedford Lakeview</option>
						<option value="Birmingham">Birmingham</option>
						<option value="Brighton">Brighton</option>
						<option value="Bristol">Bristol</option>
						<option value="Cardiff">Cardiff</option>
						<option value="Dublin">Dublin</option>
						<option value="Durrington">Durrington</option>
						<option value="Glasgow">Glasgow</option>
						<option value="Homeworkers">Homeworkers</option>
						<option value="Liverpool">Liverpool</option>
						<option value="London Whittington">London Whitington</option>
						<option value="London 155 Fenchurch St">London 155 Fenchurch St</option>
						<option value="London Eastcheap">London Eastcheap</option>
						<option value="London 107 Leadenhall">London 107 Leadenhall</option>
						<option value="London Winchester Walk">London Winchester Walk</option>
						<option value="Manchester">Manchester</option>
						<option value="Norwich">Norwich</option>
						<option value="Perth">Perth</option>
						<option value="Poole">Poole</option>
						<option value="Richmond">Richmond</option>
						<option value="Wakefield">Wakefield</option>	  
					</select>
				</div>
				<br>
				
				<input type="text" id="Location" name="Location" class="form-control" placeholder="Enter specific location is appropriate">	
				<br>
				
				<input type="submit" value="Submit" class="btn btn-success btn-md">
				<br>
				</div>
			</div>
	 </form>
 </div> 