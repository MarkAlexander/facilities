    <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="Help" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">Guidance Notes on the Approval Process</h3>
          </div>
          <div class="modal-body">
            <p>All parties involved in an insurance transaction must be approved by the Security &amp; Counterparty Committee.  The only exceptions are RKHIS and RKHIB Trade Credit &amp; Bonds direct UK based policyholders (who are not Ultra High Net Worth), and RKHIB or BIB wholesale (re)insureds who are not clients.</p> 
            <p>No lines should be bound or binding quotations obtained until all parties are approved and have their accounts activated on our systems.  Insurers who satisfy the Group's Security Criteria may be used where provisional approval has been given by the Security Officer.</p>
            <p>For the avoidance of doubt, a Third Party is an individual or corporate entity involved in the (re)insurance transaction who is neither the insured/policyholder nor the (re)insurance carrier.</p>
            
            <h4>How to check whether an account is approved</h4>
            <p>For a quick check, refer to the lists under the Group Security section of the intranet.</p>
            <p>For a more thorough search, check the processing system used in your area (e.g. Sector, IBS, Global).  Be careful to ensure that you search for the full legal name and office location.  If there is no record of the account or the record has been closed, the account requires approval.  If the account has been blocked, please contact Business Risk.</p>
            
            <h4>What is required before approving a new account?</h4>
            <p>This depends on the type of Account, different information and supporting information will be required.  You will be prompted to provide the required details and upload documents as you complete the Account Request &amp; Approval Form.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    