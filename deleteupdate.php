<!DOCTYPE html>
<html lang="en">
<head>
<title>Facilities helpdesk</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/datepicker.css">
<!-- <link href="css/bootstrap-theme.min.css" rel="stylesheet"> -->
<link href="css/styles.css" rel="stylesheet">
<script type="text/javascript" src="//use.typekit.net/vjr8xic.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/bootstrap.js"></script>

<body>
<div class="container" style="width: 100%; height: 100%; padding-right: 20px" align="float: left">
<?php
$RealID = isset($_POST["RealID"]) ? $_POST["RealID"] : '';
?>
<H1>Are you sure you want to delete?</h1>
<table cellpadding="0" cellspacing="0"><tr><td>
<form method="POST" action="deleteupdate2.php">
<input type="submit" value="Yes, Delete">
<input type="hidden" id="RealID" name="RealID" class="form-control"  value="<?php echo $RealID; ?>">
</form>
</td>
<td>
<form method="POST" action="edit.php">
<input type="submit" value="No, go back">
<input type="hidden" id="ID" name="ID" class="form-control"  value="<?php echo $RealID; ?>">
</form>
</td></tr>
</table>
</div>
</body>
</html>




