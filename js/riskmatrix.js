/**
 * Sanctions Checking Rating Matrix
 * 
 * Sanctions Checking Rating Matrix to identify accounts that require
 * sanctions checking of Secondary Interests. Score of 1 or more requires
 * deep dive sanctions check.
 */


var territories = 0;


/*
 * 
 */
function addTerritory() {
  
  territories++;
  
  $( '.territories' ).append(
    '<div class="row">' +
      '<div class="col-xs-1"></div>' +
      '<div class="col-xs-6">' +
        '<select name="territories[]" class="form-control territory" required>' +
          '<option value="none"></option>' +
          countryCPIoptions +
        '</select> ' +
      '</div>' +
      '<div class="col-xs-5">' +
        '<div class="weight input-group">' +
          '<input name="territoryWeights[]" for="territories[]" type="number" value="100" min="1" max="100" class="form-control weight" required/>' +
          '<div class="input-group-addon">%</div>' +
        '</div>' +
      '</div>' +
    '</div>'
  );
  
  // Adjust territory weights
  //$( 'input[name="territoryWeights[]"]' ).val( Math.round( 100 / territories ) );
  
  $( '.territories select' ).change( matrixUpdate );
  $( '.territories select' ).change( emptyTerritoriesUpdate );
  $( '.territories input' ).change( matrixUpdate );
  
  if ( territories == 1 ) {
    $( '.del-territory' ).prop( 'disabled', true );
    $( '.territories .weight' ).hide();
  } else {
    $( '.del-territory' ).prop( 'disabled', false );
    $( '.territories .weight' ).show();
  }
  
  emptyTerritoriesUpdate();
  matrixUpdate();
  
  return true;
  
}


/*
 * 
 */
function delTerritory() {
  
  if ( territories > 1 ) {
    territories--;
    $( '.territories .row' ).last().remove();
  }
  
  if (territories == 1) {
    $( '.del-territory' ).prop( 'disabled', true );
    $( '.territories .weight' ).hide();
    $( '#totalWeight' ).val( 100 );
  } else {
    $( '.del-territory' ).prop( 'disabled', false );
    $( '.territories .weight' ).show();
  }
  
  // Adjust territory weights
  //$( 'input[name="territoryWeights[]"]' ).val( Math.round( 100 / territories ) );
  matrixUpdate();
  emptyTerritoriesUpdate();
  
}


// Update number of empty territories
function emptyTerritoriesUpdate() {
  
  emptyTerritories = 0;
  $( 'select[name="territories[]"] option:selected' ).each( function() {
    if ( $( this ).val() == 'none' ) {
      emptyTerritories++;
    }
  });
  
  $( '#emptyTerritories' ).val( emptyTerritories );
  
}


// Update Matrix values
function matrixUpdate() {
  
  var cpis = [];  
  $( 'select[name="territories[]"] option:selected' ).each( function() {
    cpis.push( countryRisk( Number( $( this ).val() )));
  });
  
  var weights = [];
  $( 'input[name="territoryWeights[]"]' ).each( function() {
    weights.push( Number( $( this ).val() ));
  });
  
  var totalWeight = 0;
  for ( i = 0; i < weights.length; i++ ) {
    totalWeight += weights[i];
  }
  
  // Set total weight for validation
  $( '#totalWeight' ).val( totalWeight );
  
  var score = 0;
  // Percentages must sum 100
  if ( totalWeight != 100 ) {
    score = NaN;
  }
  
  for ( i = 0; i < cpis.length; i++ ) {
    score += cpis[i] * weights[i] / totalWeight;
  }
  score += Number( $( '.cob option:selected' ).val() );
  score += Number( $( '.hrlob option:selected' ).val() );
  score += Number( $( '.ownership option:selected' ).val() );
  score = Math.round( score * 100 ) / 100;
  
  if ( isNaN( score ) ) {
    $( '.panel-score .score' ).html( '-' );
    $( '.panel-score .status' ).html( '' );
  } else {
    var status = score < 1 ? 'Low Risk' : 'High Risk';
    $( '.panel-score .score' ).html( score );
    $( '.panel-score .status' ).html( status );
  }
  
  return score;
  
}


// Gets the risk associated to a country by its CPI
function countryRisk( cpi ) {
  
  // CPI > 6
  if ( cpi >= 6 )
    return 0;
  // CPI 4-6
  else if ( cpi < 6 && cpi >= 4 )
    return 0.4;
  // CPI 2-3
  else if ( cpi < 4 && cpi >= 2 )
    return 0.5;
  // CPI 1 or unrated
  else if ( cpi < 2 )
    return 0.8;
  else
    return NaN;
  //TODO: Sanctioned Territory OFAC and/or EU?
  
}


//
var countryCPIoptions =
  '<option value="1.2">Afghanistan (1.2)</option>' +
  '<option value="3.3">Albania (3.3)</option>' +
  '<option value="3.6">Algeria (3.6)</option>' +
  '<option value="-1">Andorra (-)</option>' +
  '<option value="1.9">Angola (1.9)</option>' +
  '<option value="-1">Anguilla (-)</option>' +
  '<option value="-1">Antigua and Barbuda (-)</option>' +
  '<option value="3.4">Argentina (3.4)</option>' +
  '<option value="3.7">Armenia (3.7)</option>' +
  '<option value="-1">Aruba (-)</option>' +
  '<option value="8">Australia (8)</option>' +
  '<option value="7.2">Austria (7.2)</option>' +
  '<option value="2.9">Azerbaijan (2.9)</option>' +
  '<option value="7.1">Bahamas (7.1)</option>' +
  '<option value="4.9">Bahrain (4.9)</option>' +
  '<option value="2.5">Bangladesh (2.5)</option>' +
  '<option value="7.4">Barbados (7.4)</option>' +
  '<option value="3.1">Belarus (3.1)</option>' +
  '<option value="7.6">Belgium (7.6)</option>' +
  '<option value="-1">Belize (-)</option>' +
  '<option value="3.9">Benin (3.9)</option>' +
  '<option value="-1">Bermuda (-)</option>' +
  '<option value="6.5">Bhutan (6.5)</option>' +
  '<option value="3.5">Bolivia (3.5)</option>' +
  '<option value="3.9">Bosnia and Herzegovina (3.9)</option>' +
  '<option value="6.3">Botswana (6.3)</option>' +
  '<option value="4.3">Brazil (4.3)</option>' +
  '<option value="-1">British Virgin Islands (-)</option>' +
  '<option value="0">Brunei (0)</option>' +
  '<option value="-1">Brunei Darussalam (-)</option>' +
  '<option value="4.3">Bulgaria (4.3)</option>' +
  '<option value="3.8">Burkina Faso (3.8)</option>' +
  '<option value="-1">Burma (-)</option>' +
  '<option value="2">Burundi (2)</option>' +
  '<option value="2.1">Cambodia (2.1)</option>' +
  '<option value="2.7">Cameroon (2.7)</option>' +
  '<option value="8.1">Canada (8.1)</option>' +
  '<option value="5.7">Cape Verde (5.7)</option>' +
  '<option value="-1">Cayman Islands (-)</option>' +
  '<option value="2.4">Central African Republic (2.4)</option>' +
  '<option value="2.2">Chad (2.2)</option>' +
  '<option value="7.3">Chile (7.3)</option>' +
  '<option value="3.6">China (3.6)</option>' +
  '<option value="3.7">Colombia (3.7)</option>' +
  '<option value="2.6">Comoros (2.6)</option>' +
  '<option value="2.3">Congo Republic (2.3)</option>' +
  '<option value="5.4">Costa Rica (5.4)</option>' +
  '<option value="3.2">Cote d\'Ivoire (3.2)</option>' +
  '<option value="4.8">Croatia (4.8)</option>' +
  '<option value="4.6">Cuba (4.6)</option>' +
  '<option value="6.3">Cyprus (6.3)</option>' +
  '<option value="5.1">Czech Republic (5.1)</option>' +
  '<option value="2.2">Democratic Republic of the Congo (2.2)</option>' +
  '<option value="9.2">Denmark (9.2)</option>' +
  '<option value="3.4">Djibouti (3.4)</option>' +
  '<option value="5.8">Dominica (5.8)</option>' +
  '<option value="3.2">Dominican Republic (3.2)</option>' +
  '<option value="-1">East Timor (Timor-Leste) (-)</option>' +
  '<option value="3.3">Ecuador (3.3)</option>' +
  '<option value="3.7">Egypt (3.7)</option>' +
  '<option value="3.9">El Salvador (3.9)</option>' +
  '<option value="0">Equatorial Guinea (0)</option>' +
  '<option value="1.8">Eritrea (1.8)</option>' +
  '<option value="6.9">Estonia (6.9)</option>' +
  '<option value="3.3">Ethiopia (3.3)</option>' +
  '<option value="-1">Fiji (-)</option>' +
  '<option value="8.9">Finland (8.9)</option>' +
  '<option value="6.9">France (6.9)</option>' +
  '<option value="3.7">Gabon (3.7)</option>' +
  '<option value="2.9">Gambia (2.9)</option>' +
  '<option value="5.2">Georgia (5.2)</option>' +
  '<option value="7.9">Germany (7.9)</option>' +
  '<option value="4.8">Ghana (4.8)</option>' +
  '<option value="-1">Gibraltar (-)</option>' +
  '<option value="4.3">Greece (4.3)</option>' +
  '<option value="-1">Grenada (-)</option>' +
  '<option value="-1">Grenadines (-)</option>' +
  '<option value="-1">Guam (-)</option>' +
  '<option value="3.2">Guatemala (3.2)</option>' +
  '<option value="-1">Guernsey (-)</option>' +
  '<option value="2.5">Guinea (2.5)</option>' +
  '<option value="1.9">Guinea-Bissau (1.9)</option>' +
  '<option value="3">Guyana (3)</option>' +
  '<option value="1.9">Haiti (1.9)</option>' +
  '<option value="-1">Holy See (-)</option>' +
  '<option value="2.9">Honduras (2.9)</option>' +
  '<option value="7.4">Hong Kong (7.4)</option>' +
  '<option value="5.4">Hungary (5.4)</option>' +
  '<option value="7.9">Iceland (7.9)</option>' +
  '<option value="3.8">India (3.8)</option>' +
  '<option value="3.4">Indonesia (3.4)</option>' +
  '<option value="2.7">Iran (2.7)</option>' +
  '<option value="1.6">Iraq (1.6)</option>' +
  '<option value="7.4">Ireland (7.4)</option>' +
  '<option value="-1">Isle of Man (-)</option>' +
  '<option value="6">Israel (6)</option>' +
  '<option value="4.3">Italy (4.3)</option>' +
  '<option value="3.8">Jamaica (3.8)</option>' +
  '<option value="7.6">Japan (7.6)</option>' +
  '<option value="-1">Jersey (-)</option>' +
  '<option value="4.9">Jordan (4.9)</option>' +
  '<option value="2.9">Kazakhstan (2.9)</option>' +
  '<option value="2.5">Kenya (2.5)</option>' +
  '<option value="-1">Kiribati (-)</option>' +
  '<option value="0.8">Korea (North) (0.8)</option>' +
  '<option value="5.5">Korea (South) (5.5)</option>' +
  '<option value="3.3">Kosovo (3.3)</option>' +
  '<option value="4.4">Kuwait (4.4)</option>' +
  '<option value="2.7">Kyrgyzstan (2.7)</option>' +
  '<option value="2.5">Laos (2.5)</option>' +
  '<option value="5.5">Latvia (5.5)</option>' +
  '<option value="2.7">Lebanon (2.7)</option>' +
  '<option value="4.9">Lesotho (4.9)</option>' +
  '<option value="3.7">Liberia (3.7)</option>' +
  '<option value="1.8">Libya (1.8)</option>' +
  '<option value="-1">Liechtenstein (-)</option>' +
  '<option value="5.8">Lithuania (5.8)</option>' +
  '<option value="8.2">Luxembourg (8.2)</option>' +
  '<option value="-1">Macau (-)</option>' +
  '<option value="-1">Macedonia (-)</option>' +
  '<option value="-1">Macedonia (Former Yugoslav Republic of) (-)</option>' +
  '<option value="2.8">Madagascar (2.8)</option>' +
  '<option value="3.3">Malawi (3.3)</option>' +
  '<option value="5.2">Malaysia (5.2)</option>' +
  '<option value="-1">Maldives (-)</option>' +
  '<option value="3.2">Mali (3.2)</option>' +
  '<option value="5.5">Malta (5.5)</option>' +
  '<option value="-1">Marshall Islands (-)</option>' +
  '<option value="3">Mauritania (3)</option>' +
  '<option value="5.4">Mauritius (5.4)</option>' +
  '<option value="3.5">Mexico (3.5)</option>' +
  '<option value="3.5">Moldova (3.5)</option>' +
  '<option value="-1">Monaco (-)</option>' +
  '<option value="3.9">Mongolia (3.9)</option>' +
  '<option value="4.2">Montenegro (4.2)</option>' +
  '<option value="3.9">Morocco (3.9)</option>' +
  '<option value="3.1">Mozambique (3.1)</option>' +
  '<option value="2.1">Myanmar (2.1)</option>' +
  '<option value="4.9">Namibia (4.9)</option>' +
  '<option value="-1">Nauru (-)</option>' +
  '<option value="2.9">Nepal (2.9)</option>' +
  '<option value="8.3">Netherlands (8.3)</option>' +
  '<option value="-1">Netherlands Antilles (-)</option>' +
  '<option value="9.1">New Zealand (9.1)</option>' +
  '<option value="2.8">Nicaragua (2.8)</option>' +
  '<option value="3.5">Niger (3.5)</option>' +
  '<option value="2.7">Nigeria (2.7)</option>' +
  '<option value="8.6">Norway (8.6)</option>' +
  '<option value="4.5">Oman (4.5)</option>' +
  '<option value="2.9">Pakistan (2.9)</option>' +
  '<option value="-1">Palau (-)</option>' +
  '<option value="-1">Palestinian Territories (-)</option>' +
  '<option value="3.7">Panama (3.7)</option>' +
  '<option value="2.5">Papua New Guinea (2.5)</option>' +
  '<option value="2.4">Paraguay (2.4)</option>' +
  '<option value="3.8">Peru (3.8)</option>' +
  '<option value="3.8">Philippines (3.8)</option>' +
  '<option value="6.1">Poland (6.1)</option>' +
  '<option value="6.3">Portugal (6.3)</option>' +
  '<option value="6.3">Puerto Rico (6.3)</option>' +
  '<option value="6.9">Qatar (6.9)</option>' +
  '<option value="4.3">Romania (4.3)</option>' +
  '<option value="2.7">Russia (2.7)</option>' +
  '<option value="4.9">Rwanda (4.9)</option>' +
  '<option value="-1">Saint Kitts and Nevis (-)</option>' +
  '<option value="0">Saint Lucia (0)</option>' +
  '<option value="6.7">Saint Vincent and the Grenadines (6.7)</option>' +
  '<option value="5.2">Samoa (5.2)</option>' +
  '<option value="-1">San Marino (-)</option>' +
  '<option value="4.2">Sao Tome and Principe (4.2)</option>' +
  '<option value="-1">Sark (-)</option>' +
  '<option value="4.9">Saudi Arabia (4.9)</option>' +
  '<option value="4.3">Senegal (4.3)</option>' +
  '<option value="4.1">Serbia (4.1)</option>' +
  '<option value="5.5">Seychelles (5.5)</option>' +
  '<option value="3.1">Sierra Leone (3.1)</option>' +
  '<option value="8.4">Singapore (8.4)</option>' +
  '<option value="5">Slovakia (5)</option>' +
  '<option value="5.8">Slovenia (5.8)</option>' +
  '<option value="-1">Solomon Islands (-)</option>' +
  '<option value="0.8">Somalia (0.8)</option>' +
  '<option value="4.4">South Africa (4.4)</option>' +
  '<option value="1.5">South Sudan (1.5)</option>' +
  '<option value="6">Spain (6)</option>' +
  '<option value="3.8">Sri Lanka (3.8)</option>' +
  '<option value="1.1">Sudan (1.1)</option>' +
  '<option value="3.6">Suriname (3.6)</option>' +
  '<option value="4.3">Swaziland (4.3)</option>' +
  '<option value="8.7">Sweden (8.7)</option>' +
  '<option value="8.6">Switzerland (8.6)</option>' +
  '<option value="2">Syria (2)</option>' +
  '<option value="6.1">Taiwan (6.1)</option>' +
  '<option value="2.3">Tajikistan (2.3)</option>' +
  '<option value="3.1">Tanzania (3.1)</option>' +
  '<option value="3.8">Thailand (3.8)</option>' +
  '<option value="4.5">The FYR of Macedonia (4.5)</option>' +
  '<option value="2.8">Timor-Leste (2.8)</option>' +
  '<option value="2.9">Togo (2.9)</option>' +
  '<option value="-1">Tonga (-1)</option>' +
  '<option value="3.8">Trinidad and Tobago (3.8)</option>' +
  '<option value="4">Tunisia (4)</option>' +
  '<option value="4.5">Turkey (4.5)</option>' +
  '<option value="1.7">Turkmenistan (1.7)</option>' +
  '<option value="-1">Turks and Caicos (-)</option>' +
  '<option value="-1">Tuvalu (-)</option>' +
  '<option value="2.6">Uganda (2.6)</option>' +
  '<option value="2.6">Ukraine (2.6)</option>' +
  '<option value="7">United Arab Emirates (7)</option>' +
  '<option value="7.8">United Kingdom (7.8)</option>' +
  '<option value="7.4">United States (7.4)</option>' +
  '<option value="7.3">Uruguay (7.3)</option>' +
  '<option value="-1">US Virgin Islands (-)</option>' +
  '<option value="1.8">Uzbekistan (1.8)</option>' +
  '<option value="-1">Vanuatu (-)</option>' +
  '<option value="1.9">Venezuela (1.9)</option>' +
  '<option value="3.1">Vietnam (3.1)</option>' +
  '<option value="1.9">Yemen (1.9)</option>' +
  '<option value="3.8">Zambia (3.8)</option>' +
  '<option value="2.1">Zimbabwe (2.1)</option>';
