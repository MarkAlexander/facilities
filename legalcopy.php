<?php
$userLogged=$_SERVER ['REMOTE_USER'];
$dbfilename = $_SERVER["DOCUMENT_ROOT"] . '\legal\esosa.mdb';
$dbuser = "";
$dbpassword = "";
if (!file_exists($dbfilename)) {
die("Could not find database file.");
}
$conn = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)};Dbq=$dbfilename", $dbuser, $dbpassword);
$select = $conn->prepare("SELECT * from mainquery");
$select->execute();
?>
	
<?php
include 'datalogin.php'; 
$whoisit =  $_SERVER['REMOTE_USER'];
IF (preg_match('/^r/i', $whoisit))
{$whoisit2 = substr($whoisit, 4); 
$whoisit3 = str_replace("'","''",$whoisit2);}

IF (preg_match('/^h/i', $whoisit))
{$whoisit2 = substr($whoisit, 9); 
$whoisit3 = str_replace("'","''",$whoisit2);}
$result = mysqli_query($con,"SELECT * FROM intranetusers WHERE loginname = '$whoisit3'");
while($row = mysqli_fetch_array($result))
{
IF ($_POST['ddi'] == "") { $userddi = $row['ddi'];} ELSE {$userddi = $_POST['ddi'];}
IF ($_POST['email'] == "") { $useremail = $row['useremail'];} ELSE {$useremail = $_POST['email'];}
IF ($_POST['mobile'] == "") { $usermobile = $row['mobile'];} ELSE {$usermobile = $_POST['mobile'];}
IF ($_POST['Name'] == "") { $username = $row['firstname'] . " " . $row['lastname'];} ELSE {$username = $_POST['Name'];}
IF ($_POST['jobtitle'] == "") { $userjobtitle = $row['jobtitle'];} ELSE {$userjobtitle = $_POST['jobtitle'];}
IF ($_POST['location'] == "") { $userlocation = $row['locationid'];} ELSE {$userlocation = $_POST['location'];}
IF ($_POST['ddi'] == "") { $usercompany = $row['usercomp'];} ELSE {$usercompany = $_POST['ddi'];}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Facilities helpdesk</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/datepicker.css">
<!-- <link href="css/bootstrap-theme.min.css" rel="stylesheet"> -->
<link href="css/styles.css" rel="stylesheet">
<script type="text/javascript" src="//use.typekit.net/vjr8xic.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/bootstrap.js"></script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>

<style>

.p {padding: 140px;}

</style>

</head>

<body>
<div class="container" style="width: 100%; height: 100%">

	<h1><img border="0" src="img/hyplogo.png" width="400">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> Facilities Helpdesk </strong></h1>

	<br> 
	
	<div class="panel panel-default panel-preview" style="width:35%; float: left; min-width: 300px; max-width: 500px; margin-right: 20px;">
      
	  <div class="panel-heading">
        <h3 class="panel-title">New Helpdesk Request</h3>
      </div>
	  <form method="POST" action="insert.php">
	  <div class="panel-body">
			<div class="col-md-6">
  

<BR>

  
<p><B>Category of request</b></p>
		  <div class="dropdown">  
  <select id="Company" name="Company" class="form-control">
	  
	  
	<option selected>Select from list</option>
<option value="Archive System">Archive System</option>
<option value="Cleaning">Cleaning</option>
<option value="Climate Control">Climate Control</option>
<option value="Couriers/Post">Couriers/Post</option>
<option value="Decoration">Decoration</option>
<option value="Electrical">Electrical </option>
<option value="External Areas">External Areas</option>
<option value="Fixtures, Fittings &amp; Equipment">Fixtures, Fittings & Equipment</option>
<option value="Health & Safety">Health & Safety</option>
<option value="Lifts">Lifts</option>
<option value="Office Consumables">Office Consumables</option>
<option value="Plumbing">Plumbing</option>
<option value="Print Device - Other">Print Device - Other</option>
<option value="Print Device - Ricoh">Print Device - Ricoh</option>
<option value="Print Device - Xerox">Print Device - Xerox</option>
<option value="Security">Security</option>
<option value="Staff Relocation">Staff Relocation</option>
<option value="Stationery">Stationery</option>
<option value="Vending">Vending</option>  
	  
</select>
</div>
	  
	<BR> 
	  
	  

<p><B>Brief Description of Request</b></p>
		<textarea id="Request" name="Request" class="form-control" rows="2">		</textarea>



<BR>




<p><B>Building</b></p>
		  <div class="dropdown">  
  <select id="Building" name="Building" class="form-control">
	  
	<option value="Bedford">Bedford Woodlands</option>
<option value="BedfordLakeview">Bedford Lakeview</option>
				<option value="Bermuda">Bermuda</option>
				<option value="Birmingham">Birmingham</option>
<option value="Brighton">Brighton</option>
<option value="Cardiff">Cardiff</option>
<option value="Dublin">Dublin</option>
				<option value="Durrington">Durrington</option>
				<option value="Glasgow">Glasgow</option>
				<option value="Leeds">Leeds</option>
<option value="Liverpool">Liverpool</option>
				<option value="London Whittington">London Whitington</option>
				<option value="London 71 Fenchurch St">London 71 Fenchurch St</option>
				<option value="London 155 Fenchurch St">London 155 Fenchurch St</option>
				<option value="London Eastcheap">London Eastcheap</option>
				<option value="London 107 Leadenhall">London 107 Leadenhall</option>
<option value="Manchester">Manchester</option>
<option value="Norwich">Norwich</option>
				<option value="Peterborough">Peterborough</option>
				<option value="Poole">Poole</option>
				<option value="Redhill">Redhill</option>
				<option value="Richmond">Richmond</option>
<option value="Wakefield">Wakefield</option>	  
 	  
</select>
</div>


<BR>

	
<p><b>Location</b> (e.g. First Floor Landing)</p>
		<input type="Location" id="Location" name="Location" class="form-control">	

<BR><BR>
	
		<input type="submit" value="Submit" class="btn btn-success btn-md">
	
	
	  </div>
	 </div>
	 </form>
	 </div> 
	
	
	
	
	
	
	
	<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	 <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Guidance Notes</h4>
      </div>
      <div class="modal-body">
		<b>[S] Strategic materiality:</b> Strategically material contracts are those that may affect the strategic direction of the business and those that may affect the ability of the Group to carry on its business.<br>
	<br>
	<b>[R] Reputational materiality:</b> contracts material to reputation are those that may affect the perception of the insurance market, regulators, clients, competitors and suppliers of the Group, or which involve the processing of personal data (as defined by data protection legislation).<br>
	<br>
	<b>[F] Financial materiality</b> Contracts material in financial terms are demmed by the Group to be those with an annual value of £25,000 or greater.<br>
	<br>
	<b>Outsourcing:</b> the provision by a supplier of material goods and/or services to the Group:
	<ol type="i">
		<li>which were previously or would commonly* be part of the broking business of the Group (eg. claims handling, renewal date gathering); or</li>
		<li>which, where not part of the broking business of the Group, would nevertheless ordinarily* be retained in-house (eg. Training, Internal Audit, File Review, HR, Facilities, IT); or</li>
		<li>which if undertaken by the Group would be subject to edxternal regulation (eg. claims handling).</li>
	</ol>
	(* in a Group of the scale and complexity of the R K Harrison Group of companies (the 'Group'))
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
	
	
</div>



<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>

   <script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('#ContractDate').datepicker({
                    format: "dd/mm/yyyy"
                });  
				$('#ComDate').datepicker({
                    format: "dd/mm/yyyy"
                }); 
				$('#ExpiryDate').datepicker({
                    format: "dd/mm/yyyy"
                });
            });
        </script>


</body>
</html>