<!DOCTYPE html>
<html lang="en">
<head>
<title>Facilities helpdesk</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/datepicker.css">
<!-- <link href="css/bootstrap-theme.min.css" rel="stylesheet"> -->
<link href="css/styles.css" rel="stylesheet">
<script type="text/javascript" src="//use.typekit.net/vjr8xic.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/bootstrap.js"></script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>

<style>

.p {padding: 140px;}
.oneValue {background: #e6e6e6;}
.anotherValue {background: #f2f2f2;}
</style>

</head>

<body>
<div class="container" style="width: 100%; height: 100%">
<h1>Insert Document</h1>
<?php
$RealID = isset($_POST["RealID"]) ? $_POST["RealID"] : '';
  echo "<form action='fileupload2.php' enctype='multipart/form-data' method='post'>";  
  echo "<B>Document to be uploaded</B><p class='adminnotes'>Browse to the document you wish to upload</p><input name='uploaded' type='file' size='50'>";
  echo "<BR><BR><B>Document Name</B><p class='adminnotes'>Title of the document.  Please keep it short and precise.</p><input type='text' name='librarydoc'>";
  echo "<BR><BR><input type='submit' class='btnSave'/><input type='hidden' name='RealID' id='RealID' value='" . $RealID ."'></form>";
?>
</div>
</body>
</html>