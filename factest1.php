<?php
                ini_set('display_errors', 'On');
                error_reporting(E_ALL | E_STRICT);
                
                $dbfilename = $_SERVER["DOCUMENT_ROOT"] . './facilities/copy/facilitiescopy.mdb';
                $dbuser = "";
                $dbpassword = "";
                if (!file_exists($dbfilename)) {
                                die("Could not find database file.");
                }
                $conn = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)};Dbq=$dbfilename", $dbuser, $dbpassword);

?>

<?php
$TrueID = $_REQUEST['ID'];
$sql = "SELECT ID, Your_Name, Short_Desc, Full_Desc, Location, floor, owner, Priority, Cost_Centre, Cost_Amount, Contractor, Follow_up_date, securityyes, securityno, hsyes, hsno, securityincident, healthsafety, mainCat, subCat from Table1 WHERE ID = 22842";
$result = $conn->query($sql); 
                                                            
                                                                while($row = $result->fetch()) {
																$RealID = $row['ID'];
																$Your_Name = $row['Your_Name'];
																$Short_Desc = $row['Short_Desc'];
																$Full_Desc = $row['Full_Desc'];
																$Location = $row['Location'];
															    $floor = $row['floor'];
																$owner = $row['owner'];
																$Priority = $row['Priority'];
																$Cost_Centre = $row['Cost_Centre'];
																$Cost_Amount = $row['Cost_Amount'];
																$Contractor = $row['Contractor'];
																$Follow_up_date = $row['Follow_up_date'];
																$securityyes = $row['securityyes'];
																$securityno = $row['securityno'];
																$hsno = $row['hsno'];
																$hsyes = $row['hsyes'];
																$securityincident = $row['securityincident'];
																$healthsafety = $row['healthsafety'];
																$mainCat = $row['mainCat'];
																$subCat = $row['subCat'];
															    }  

?> 
    
<!DOCTYPE html>
<html lang="en">
<head>
<title>Facilities helpdesk</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/datepicker.css">
<!-- <link href="css/bootstrap-theme.min.css" rel="stylesheet"> -->
<link href="css/styles.css" rel="stylesheet">
<script type="text/javascript" src="//use.typekit.net/vjr8xic.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/bootstrap.js"></script>
<script type="text/javascript">
function fetch_select(val)
{
 $.ajax({
 type: 'post',
 url: 'fetch_data.php',
 data: {
  get_option:val
 },
 success: function (response) {
  document.getElementById("new_select").innerHTML=response; 
 }
 });
}

</script>
<script type="text/javascript">
function fetch_select2(val)
{
 $.ajax({
 type: 'post',
 url: 'fetch_data2.php',
 data: {
  get_option2:val
 },
 success: function (response) {
  document.getElementById("new_select2").innerHTML=response; 
 }
 });
}

</script>




<style>
.p {padding: 140px;}
.table-bordered {padding: 0px; margin: 0px; cell-padding: 0px}
</style>
</head>

<body>

<?php include "header.php" ?>



<div class="container" style="width: 100%; height: 100%; padding: 20px 20px" align="float: left">

	
	<div class="panel panel-default panel-preview" style="width:100%; min-width: 300px; max-width:1500px; float: left;">
      
	  <div class="panel-heading">
        <h3 class="panel-title">Edit Helpdesk Request</h3>
      </div>
	  
	  
	  
	  
	  
	  
	  
	  <DIV align="left" style="padding: 25px 45px">  



	 
</div>

	  
	  



	  
	  
	  <form method="POST" action="editupdate.php">
	  <div class="panel-body" style="clear: both;">
			
<div class="col-md-12">	
<div class="col-md-6">	  
  
    <input type="hidden" id="RealID" name="RealID" class="form-control"  value="<?php echo $RealID; ?>">


  
<p><B>Category of request</b></p>
		  <div class="dropdown">  
		  
		  
		  <?php
		  
					$sql2 = "SELECT categoryID, categoryName from category";
					$result2 = $conn->query($sql2); 
					echo '<select onchange="fetch_select(this.value);" id="mainCat" name="mainCat" class="form-control">'; // Open your drop down box
					// Loop through the query results, outputing the options one by one
					
					while($row2 = $result2->fetch()) {
						
					   echo '<option ';
					   IF ($row2['categoryID'] == $mainCat) {echo 'selected ';}
					   echo 'value="';
					   echo $row2['categoryID'];
					   echo '">';
					   echo $row2['categoryName'];
					   echo '</option>';
					}
					echo '</select>';// Close your drop down box    
				?> 
		  
		  
		  
</div>
	  
	<BR>
	
	  <p><B>Sub Category</b></p>
	  		
	

	  
	  
	  
	  
	  
	  
		  <div class="dropdown">  
		   <select id="new_select" onchange="fetch_select2(this.value);" name="roomid" value="idrooms" class="form-control">
		   
		   <?php
		   $sql3 = "SELECT subcatID, subcatName, categoryOwner from subcategory WHERE categoryOwner = " . $mainCat;
					$result3 = $conn->query($sql3); 
					echo '<option>Select from list</option>';
		   
		   while($row3 = $result3->fetch()) {
						
					   echo '<option ';
					   IF ($row3['subcatID'] == $subCat) {echo 'selected ';}
					   echo 'value="';
					   echo $row3['subcatID'];
					   echo '">';
					   echo $row3['subcatName'];
					   echo '</option>';
					}
					
					?>
		   
		   
		   </select>
		  </div>
	  
	
	  
	

	
		<input type="submit" value="Update Record" class="btn btn-success btn-md" style="align:center">
		<input type="hidden" id="owner2" name="owner2" class="form-control"  value="<?php echo $owner ?>">
	
	 </div>
	</div>
	</div>
	</form>
	</div>
	
	<DIV align="left" style="padding: 10px 0px; clear: both;">  
	
	</DIV>  
	
	
	
</div>
	
	
  
  
  


  
 


</body>
</html>